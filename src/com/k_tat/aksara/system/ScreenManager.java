package com.k_tat.aksara.system;

import java.util.concurrent.CopyOnWriteArrayList;

import android.graphics.Canvas;
import android.view.MotionEvent;

public class ScreenManager {
	private CopyOnWriteArrayList<IScreen> screens; 
	//makan memori lebih gede tapi threadsafe katanya
	//alasan diganti: kalo ngepop bakal error concurrent kalo ga pake ini
	
	public ScreenManager(){
		screens = new CopyOnWriteArrayList<IScreen>();
	}
	
	public void push(IScreen screen){
		screens.add(screen);
	}
	
	public boolean isLastScreen(){
		return screens.size() == 1;
	}
	
	public int getSize(){
		return screens.size();
	}
	
	public void pop(){
		if(!screens.isEmpty()){
			IScreen lastScreen = screens.get(screens.size()-1);
			screens.remove(screens.size()-1);
			lastScreen.dispose();
		}
	}
	
	public void draw(Canvas canvas){
		//semuanya di draw
		if(!screens.isEmpty()){
			for(IScreen s : screens){
				s.draw(canvas);
			}
		}
	}
	
	public void update(){
		//yg diupdate cuma yg top
		if(!screens.isEmpty()){
			//Log.d("harits", "yang diupdate: " + getTopScreenName());
			screens.get(screens.size()-1).update();
		}
			//Log.d("harits", "screen is empty");
	}
	
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy){
		//yg diupdate cuma yg top
		if(!screens.isEmpty()){
			screens.get(screens.size()-1).onTouchEvent(e, mag, dx, dy);
		}
	}
	
	public String getTopScreenName(){
		return screens.get(screens.size()-1).getName();
	}
	
	public void release(){
		while(!screens.isEmpty()){
			IScreen lastScreen = screens.get(screens.size()-1);
			screens.remove(screens.size()-1);
			lastScreen.dispose();
		}
	}
	
}
