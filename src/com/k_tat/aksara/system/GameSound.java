package com.k_tat.aksara.system;

import android.media.SoundPool;

public class GameSound {
    int soundId;
    SoundPool soundPool;
    

    
	public GameSound(SoundPool soundPool, int soundId) {
		this.soundPool = soundPool;
		this.soundId = soundId;
	}

    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    public void dispose() {
        soundPool.unload(soundId);
    }
}
