package com.k_tat.aksara.system;

import java.util.ArrayList;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.models.Level;

public class ResourceLoaderThread implements Runnable {
	private MainGameView game;
	private Loader parent;
	private boolean loadAssets;
	private ArrayList<String> queue;
	private Level level;
	private int loaded;
	private int enqueued;
	
	public ResourceLoaderThread(MainGameView game, Level level, Loader parent){
		this.level = level;
		this.game = game;
		this.parent = parent;
		queue = new ArrayList<String>();
		loadAssets = false;
		loaded = 0;
		enqueued = 0;
	}
	
	public int getPercent(){
		return (int) (loaded*100/(float)enqueued);
	}
	
	public void setLoadAssets(boolean val){
		loadAssets = val;
		enqueued += 10;
	}
	
	public void enqueue(String fileName){
		queue.add(fileName);
		enqueued++;
	}
	
	public void enqueueMonster(String monsterName){
		queue.add(monsterName);
		queue.add(monsterName + "_explode");
		queue.add(monsterName + "_bubble");
		String monsterColor = monsterName.split("_")[1];
		queue.add("bubble_" + monsterColor);
		enqueued += 4;
	}
	
	public void process(){
		for(String fileName: queue){
			game.initBitmap(fileName);
			loaded++;
			//Log.d("harits", "Bitmap named: " + fileName + " has been loaded.");
		}
		if(level != null)
			level.load(game);
		if(loadAssets)
			game.loadAssetData();
		System.gc();
		
		//Log.d("harits", "Resource Loader Thread process() done.");
	}
	
	@Override
	public void run() {		
		process();
		
		parent.setLoadingFinished(true);
		//Log.d("harits", "Resource Loader Thread run() done.");
	}
}
