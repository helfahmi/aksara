package com.k_tat.aksara.system;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

public class GameMusic {
	MediaPlayer mediaPlayer;
	boolean isPrepared = false;
	
	public GameMusic(AssetFileDescriptor assetDescriptor) {
		mediaPlayer = new MediaPlayer();
		try{
			//Log.d("RENUSA","Load audio");
            mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(),
                    assetDescriptor.getStartOffset(),
                    assetDescriptor.getLength());
			mediaPlayer.prepare();
			isPrepared = true;
			mediaPlayer.setLooping(false);
		} catch (IOException e){
			Log.d("RENUSA",e.getMessage());
			mediaPlayer = null;
		}
	}

	public void play(){
	    if (this.mediaPlayer.isPlaying())
	        return;
	
	    try {
	        synchronized (this) {
	            if (!isPrepared)
	                mediaPlayer.prepare();
	            mediaPlayer.start();
	        }
	    } catch (IllegalStateException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public void stop(){
		if (this.mediaPlayer.isPlaying() == true){
		    this.mediaPlayer.stop();
			synchronized (this) {
				isPrepared = false;
		    }
		}
	}

    public void pause(){
        if (this.mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    public void setLooping(boolean isLooping){
        mediaPlayer.setLooping(isLooping);
    }

    public void setVolume(float volume){
        mediaPlayer.setVolume(volume, volume);
    }

    public boolean isPlaying(){
        return this.mediaPlayer.isPlaying();
    }

    public boolean isStopped(){
        return !isPrepared;
    }

    public boolean isLooping(){
        return mediaPlayer.isLooping();
    }
    
    public boolean isPaused(){
    	return isPrepared && !mediaPlayer.isPlaying();
    }

    public void dispose(){    
	   	if (this.mediaPlayer.isPlaying()){
	   		this.mediaPlayer.stop();
	   	}
	    this.mediaPlayer.release();
    }

	public void seekBegin(){
		mediaPlayer.seekTo(0);	
	}
}
