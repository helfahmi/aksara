package com.k_tat.aksara.system;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.util.Log;

public class BitmapManager2 {
    private AssetManager assets;
    private HashMap<String, Bitmap> bitmaps;
    
    public BitmapManager2(AssetManager assets){
        this.assets = assets;  
        bitmaps = new HashMap<String, Bitmap>();
    }
    
    public void dispose(String name){
    	bitmaps.get(name).recycle();
    	bitmaps.remove(name);
    }
    
    public void release(){
        for(Bitmap b: bitmaps.values()){
        	if(b != null){
	            b.recycle();
	            b = null;
        	}
        }
    }
    
    public void initBitmap(String fileName){
    	if(bitmaps.get(fileName) != null)
    		return;
    	
    	String dir = "images";
    	
    	String fileDir = dir + "/" + fileName + ".png";
    	InputStream in = null;
        Bitmap bitmap = null;
        
        try {
            in = assets.open(fileDir);
            bitmap = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
        	try {
	            in = assets.open("images/" + fileName + ".jpg");
	            bitmap = BitmapFactory.decodeStream(in);
	        } catch (IOException ee) {
	            throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
	        }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        //Log.d("fileload", "bitmap key: " + hashKey[0]);
        
        bitmaps.put(fileName, bitmap);
    }
    
    public void initialize(){
    	String dir = "images";
    	String[] resources = null;
		try {
			//Log.d("filecheck", "start listing...");
			resources = assets.list(dir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(String fileName: resources){
			String fileDir = dir + "/" + fileName;
	    	InputStream in = null;
	        Bitmap bitmap = null;
	        try {
	        	Config config = Config.ARGB_4444;
	            Options options = new Options();
	            options.inPreferredConfig = config;
	        	
	            in = assets.open(fileDir);
	            bitmap = BitmapFactory.decodeStream(in, null, options);
	            if (bitmap == null)
	                throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
	        } catch (IOException e) {
	            throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
	        } finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException e) {
	                }
	            }
	        }
	        
	        //Log.d("fileload", "file name: " + fileName);
	        String hashKey[] = fileName.split("\\.");
	        //Log.d("fileload", "bitmap key: " + hashKey[0]);
	        
	        bitmaps.put(hashKey[0], bitmap);
    	}
    }
    
    public Bitmap get(String fileName) {
    	Bitmap result = bitmaps.get(fileName);
    	if(result == null){
	    	InputStream in = null;
	        Bitmap bitmap = null;
	        try {
	            in = assets.open("images/" + fileName +".png");
	            bitmap = BitmapFactory.decodeStream(in);
	        } catch (IOException e) {
	        	try {
		            in = assets.open("images/" + fileName + ".jpg");
		            bitmap = BitmapFactory.decodeStream(in);
		        } catch (IOException ee) {
		            throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
		        }
	        } finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException e) {
	                }
	            }
	        }
	        bitmaps.put(fileName, bitmap);
    	}
    	return bitmaps.get(fileName);
    }
}
