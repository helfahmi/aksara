package com.k_tat.aksara.system;

import android.content.res.AssetManager;

public class DBLoader {
	
	//modelnya singleton, jadi dapetin objeknya: DBLoader.getInstance()
	private static DBLoader instance = null;
	private AssetManager assets;
	
	private DBLoader(AssetManager assets){
		this.assets = assets;
	}
	
	//getter
	public static void loadAll(){
		//load semua asset
	}
	
	//statics
	public static void initialize(AssetManager assets){
		instance = new DBLoader(assets);
	}
	
	
	public static DBLoader getInstance(){
		return instance;
	}
	
	public static void release(){
		/*
		instance.elements.clear();
		...
		*/
		instance = null;
	}
}
