package com.k_tat.aksara.system;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class GameAudio {
	AssetManager assets;
	MediaPlayer mediaPlayer;
	SoundPool soundPool;
	int SoundId;
	
	public GameAudio (Activity activity){
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
	}
	
    public GameMusic createMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            return new GameMusic(assetDescriptor);
        } catch (IOException e) {
        	//Log.d("harits", "file " + filename + " not found.");
            return null;
        	//throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    public GameSound createSound(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            return new GameSound(soundPool, soundId);
        } catch (IOException e) {
            //throw new RuntimeException("Couldn't load sound '" + filename + "'");
        	return null;
        }
    }
}
