package com.k_tat.aksara.system;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;

public abstract class Screen {
        protected Context context;
        protected int realScreenWidth, realScreenHeight;
        protected int baseScreenWidth, baseScreenHeight;
        
        public Screen(Context con, int sw, int sh, int bsw, int bsh){
                context = con;
                realScreenWidth = sw;
                realScreenHeight = sh;
                baseScreenWidth = bsw;
                baseScreenHeight = bsh;
        }
        
        public abstract void update();
        public abstract void draw(Canvas canvas);
        public abstract void onTouchEvent(MotionEvent e, float mag);
}