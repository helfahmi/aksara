package com.k_tat.aksara.system;

import android.view.MotionEvent;

public interface Touchable {
	public boolean onTouchEvent(MotionEvent event, float mag, float dx, float dy);
}
