package com.k_tat.aksara.system;

import android.graphics.Canvas;
import android.view.MotionEvent;

public interface IScreen {
	public void update();
	public void draw(Canvas canvas);
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy);
	public void dispose();
	public String getName();
}
