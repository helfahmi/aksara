package com.k_tat.aksara.main;

import com.k_tat.aksara.screens.LevelScreen;
import com.k_tat.aksara.screens.MenuScreen;
import com.k_tat.aksara.screens.PauseScreen;
import com.k_tat.aksara.screens.TransitionScreen;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

@SuppressLint("NewApi")
public class MainGameActivity extends Activity {
	private DisplayMetrics metrics;	
	private MainGameView mainGameView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Log.d("harits", "masuk maingameactivity");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);		
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics); // dapetin ukuran layar
		StringBuilder sb = new StringBuilder();
		sb.append(metrics.densityDpi); //low = 120 160 240
		mainGameView = new MainGameView(this, this, metrics.widthPixels, metrics.heightPixels, metrics.densityDpi);
		//setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //pake ini error kalo awalnya udahlandscape 
		setContentView(mainGameView);	
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
    public void onResume() {
        super.onResume();
        
        //Log.d("harits", "activity is resuming");
        if(mainGameView!=null){
        	mainGameView.continueThread();
        	if(mainGameView.getScreenManager().getTopScreenName() != "SplashScreen"){
        		mainGameView.playAudio();
        	}
        	
        	if(mainGameView.getScreenManager().getTopScreenName() == "GameScreen"){
        		mainGameView.push(new PauseScreen(mainGameView));
        	}
        }
    }
	
	@Override
	protected void onPause() {
		super.onPause();
		//Log.d("harits", "activity is pausing");
		if(mainGameView!=null){
			mainGameView.pauseThread(); //matiin thread
			mainGameView.pauseAudio();
			if(isFinishing()){
				//Log.d("harits", "activity is finishing");
				mainGameView.releaseThread();
				mainGameView.dispose();
			}
		}
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		
		//Log.d("harits", "activity is stopping");
		//mainGameView.dispose();
		//mainGameView = null;
		System.gc();
		//finish();
	}
	
	@Override
	public void onBackPressed() {
		
		/*
	   if(mainGameView.isLastScreen()){
		   //mainGameView.dispose();
		   System.gc();
		   finish();
	   } else {
		   mainGameView.pop();
	   }
	   */
	   
		String screenName = mainGameView.getScreenManager().getTopScreenName();
		if(screenName == "BahasaScreen" || screenName == "LevelScreen" || screenName == "AccurationScreen" || screenName == "SettingScreen" || screenName == "GameOverScreen"){
			mainGameView.pop();
			mainGameView.push(new MenuScreen(mainGameView));
			mainGameView.push(new TransitionScreen(mainGameView));
		} else if(screenName == "PauseScreen"){
			mainGameView.pop();
		} else if(screenName == "GameScreen"){
			mainGameView.push(new PauseScreen(mainGameView));
		} else if(screenName == "MenuScreen"){
			mainGameView.pop();
			finishApp();
		} else if (screenName == "CompletedScreen" || screenName == "GameOverScreen") {
			mainGameView.pop();
			mainGameView.push(new LevelScreen(mainGameView));
			mainGameView.push(new TransitionScreen(mainGameView));
		} else if (screenName == "ConfirmScreen") {
			mainGameView.pop();
		}
	}
	
	public void finishApp(){
		//mainGameView.dispose();
		finish();
		return;
	}
}
