package com.k_tat.aksara.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.media.AudioManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.k_tat.aksara.models.Aksara;
import com.k_tat.aksara.models.CheckPoint;
import com.k_tat.aksara.models.Language;
import com.k_tat.aksara.models.Level;
import com.k_tat.aksara.models.MonsterModel;
import com.k_tat.aksara.screens.CompletedScreen;
import com.k_tat.aksara.screens.FontTestScreen;
import com.k_tat.aksara.screens.SplashScreen;
import com.k_tat.aksara.system.BitmapManager2;
import com.k_tat.aksara.system.GameAudio;
import com.k_tat.aksara.system.GameMusic;
import com.k_tat.aksara.system.IScreen;
import com.k_tat.aksara.system.ScreenManager;

@SuppressLint("ViewConstructor")
public class MainGameView extends SurfaceView implements SurfaceHolder.Callback {
	private final float standardWidth = 480.f;
	private final float standardHeight = 800.f;
	private float realScreenWidth;
	private float realScreenHeight;
	
	private Bitmap blackbar;
	private boolean useBlackBar;
	private Rect bbSrc;
	private RectF bbDstL, bbDstR;
	
	private BitmapManager2 bManager;
	private ScreenManager sManager;
	
	public GameLoop thread;
	private Matrix matrix; //matrix perbesaran canvas
	private MainGameActivity activity; //parent dari class ini
	
	public MainGameActivity getActivity() {
		return activity;
	}

	private float magnification;
	private Context context;
	private float dx, dy; //besar pergeseran layar karena blackbar
	private AssetManager assets;
	private HashMap<String, Language> languages;
	private HashMap<String, Level> levels;
	private HashMap<String, Typeface> fonts;
	private GameAudio gameAudio;
	private GameMusic menuMusic, gameMusic;
	private String globalLang;
	private String langCode;
	private String gameLangCode;
	private int currLevel; //buat level tertinggi saat ini
	private int currVolume; //buat volume sound saat ini
	private Object tmpData;
	
	public MainGameView(Context context, MainGameActivity activity, int screenWidth, int screenHeight, int screenDensity) {
		super(context);
		
		//Log.d("harits", "masuk main game view");
		getHolder().addCallback(this);
		setFocusable(true);
		
		assets = context.getApplicationContext().getAssets();
		
		this.activity = activity;
		this.context = context;
		realScreenHeight = screenHeight;
		realScreenWidth = screenWidth;
		
		//test
		/*** Initialization ***/
		globalLang = "indonesia";
		langCode = "in";
		gameLangCode = "ar";
		currLevel = 16; //masih hardcode
		currVolume = 80;
		bManager = new BitmapManager2(assets);
		blackbar = getBitmap("black_bar");
		useBlackBar = false;
		sManager = new ScreenManager();
		languages = new HashMap<String, Language>();
		levels = new HashMap<String, Level>();
		gameAudio = new GameAudio(activity);
		menuMusic = gameAudio.createMusic("audio/main_themev2.ogg");
		gameMusic = gameAudio.createMusic("audio/run_theme.ogg");
		menuMusic.setLooping(true);
		gameMusic.setLooping(true);
		
		/*** FONT ***/
		fonts = new HashMap<String, Typeface>();
		
		//Log.d("harits", "ukuran layar asli (w x h): " + screenWidth + " " + screenHeight);
		//Log.d("harits", "ukuran layar base (w x h): " + standardWidth+ " " + standardHeight);
		//pilih magnifikasi paling kecil diantara magnifikasi x dan y
		//magnification = 1; //no magnification
		magnification = (((float) screenHeight) / standardHeight) < (((float) screenWidth) / standardWidth) ? (((float) screenHeight) / standardHeight) : (((float) screenWidth) / standardWidth);
		
		//Log.d("harits", "height ratio: " + screenHeight / standardHeight);
		//Log.d("harits", "width ratio: " + screenWidth / standardWidth);
		//Log.d("harits", "mag: " + magnification);
		Matrix matrix1 = new Matrix();
		matrix1.setScale(magnification, magnification,0,0);
		Matrix matrix2 = new Matrix();
		//Log.d("harits", "screenWidth, standardWidth*mag: "+ screenWidth + ", " + standardWidth*magnification);
		dx = (screenWidth/magnification - standardWidth)/2;
		dy = (screenHeight/magnification - standardHeight)/2;
		
		bbSrc = new Rect(0, 0, 20, 20);
		
		if(dx != 0){
			bbDstL = new RectF(-dx, 0, 0, 800);
			bbDstR = new RectF(480, 0, 480 + dx, 800);
		} else {
			bbDstL = new RectF(0, -dy, 480, 0);
			bbDstR = new RectF(0, 800, 480, 800 + dy);
		}
		//Log.d("harits", "" + dx);
		matrix2.setTranslate(dx, dy);
		matrix = new Matrix();
		matrix.setConcat(matrix1, matrix2);
		//MainMenu mm = new MainMenu(context, screenWidth, screenHeight);
		//manager.push(mm);
		sManager.push(new SplashScreen(this));
	}
	
	public void setTmpData(Object obj){
		tmpData = obj;
	}
	
	public Object getTmpData(){
		return tmpData;
	}
	
	public void loadFonts(){
    	String dir = "fonts";
    	String[] resources = null;
		try {
			resources = assets.list(dir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(String fileName: resources){
			String fileDir = dir + "/" + fileName;
	    	InputStream in = null;
	        Typeface tmp = null;
	    	try {
	            in = assets.open(fileDir);
	            tmp = Typeface.createFromAsset(assets, fileDir);
	            if (tmp == null)
	                throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
	        } catch (IOException e) {
	            throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
	        } finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException e) {
	                }
	            }
	        }
	        
	        //Log.d("fileload", "file name: " + fileName);
	        String hashKey[] = fileName.split("\\.");
	        //Log.d("fileload", "bitmap key: " + hashKey[0]);
	        
	        fonts.put(hashKey[0], tmp);
    	}
    }
	
	public Paint createFontPaint(String fontName, int color, int size){
		Paint paint = new Paint();
		paint.setTextSize(size);
		paint.setColor(color);
		paint.setTypeface(fonts.get(fontName));
		return paint;
	}
	
	public int getLastLevelCompleted(){
		SharedPreferences levelInfo = activity.getSharedPreferences("LEVEL_INFO", 0);
		int result = levelInfo.getInt(getGameLang() + "level", 1);
		Log.d("harits", "Last level completed: " + result);
		
		return result;
	}
	
	public int getcurrVolume() {
		return currVolume;
	}
	
	public void setcurrVolume(int cV) {
		currVolume = cV;
		float percentVol = (float)currVolume/305f;
		Log.d("RENUSA", percentVol+"(percent)");
		Log.d("RENUSA", cV+"(currnet)");
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		Log.d("RENUSA", audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)+"");
		//kalkulasi suara : currvolume/100 * maxVolume
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int)(percentVol * audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)), 0);
	}
	
	public int getcurrLevel() {
		/*Log.d("harits", "Clearing level data.");
		SharedPreferences.Editor editor = getActivity().getSharedPreferences("LEVEL_INFO", 0).edit();
		editor.clear();
		editor.commit();*/
		return getLastLevelCompleted();
	}
	
	public void setcurrLevel(int cL) {
		// Simpan kalo level ini sudah paling selesai.
		SharedPreferences.Editor editor = getActivity().getSharedPreferences("LEVEL_INFO", 0).edit();
		editor.putInt(getGameLang() + "level", cL);
	    editor.commit();
	}
	
	public String getGameLang() {
		return gameLangCode;
	}
	
	public void setGameLangCode(String gameLangCode) {
		this.gameLangCode = gameLangCode; 
	}
	
	public String getLang(){
		return globalLang;
	}
	
	/*** Kembalikan bahasa yang dipakai untuk interface ***/
	public String getLangCode(){
		return langCode;
	}
	
	public void playGameMusic(){
		gameMusic.play();
		//gameMusic.seekBegin();
	}
	
	public void playMenuMusic(){
		menuMusic.play();
		//menuMusic.seekBegin();
	}
	
	public void stopGameMusic(){
		gameMusic.stop();
	}
	
	public void stopMenuMusic(){
		menuMusic.stop();
	}
	
	public void finish(){
		activity.finish();
		return;
	}
	
	public int getFPS(){
		return thread.getFPS();
	}
	
	public GameAudio getAudio(){
		return gameAudio;
	}
	
	public String getValue(Scanner scan){
	/* Get a line of string that is not a comment in the asset file */
		String tmpLine;
		do {
			tmpLine = scan.nextLine();
		} while(tmpLine.charAt(0) == '#');
		return tmpLine;
	}
	
	public Language getLanguage(String code){
		return languages.get(code);
	}
	
	public Level getLevel(String name){
		return levels.get(name);
	}
	
	public int getTotalLevel(){
		return levels.size();
	}
	
	public void loadAssetData(){
		try{
			/*** 1. load language data: checkpoints, etc ***/
			String line;
			StringBuilder str=new StringBuilder();
			BufferedReader buf = new BufferedReader(new InputStreamReader(assets.open("data/cpdata.dat")));
			while ((line = buf.readLine()) != null){
				str.append(line);
				str.append("\n");
			}
			
			//build semua elementnya dulu
			Scanner scan = new Scanner(str.toString());
			
			int numOfLang = Integer.parseInt(getValue(scan));
			
			for(int i=0;i<numOfLang;i++){
				
				Language tmpLang = new Language(getValue(scan));
				
				int numOfAksara = Integer.parseInt(getValue(scan));
				
				for(int j=0;j<numOfAksara;j++){
					String aksaraResName = getValue(scan);
					Aksara tmpAksara = new Aksara(aksaraResName, "sounds/" + aksaraResName + ".wav"); //nama resource
					tmpAksara.setRealName(getValue(scan));//nama sebutan aksaranya
					
					int numOfCheckpoints = Integer.parseInt(getValue(scan));
					
					for(int k=0;k<numOfCheckpoints;k++){
						String theLine = getValue(scan);
						//Log.d("harits", theLine);
						String nums[] = theLine.split(" ");
						
						int tmpX = Integer.parseInt(nums[0]);
						int tmpY = Integer.parseInt(nums[1]);
						int tmpR = Integer.parseInt(nums[2]);
						String tmpType = nums[3];//.substring(0, nums[3].length());
						
						CheckPoint cp = new CheckPoint(tmpX, tmpY, tmpR, tmpType);
						
						tmpAksara.addCheckPoint(cp);
					}
					tmpLang.addAksara(tmpAksara);
				}
				languages.put(tmpLang.getCode(), tmpLang);
			}
			
			/*** 2. load level data ***/
			
			line = "";
			StringBuilder str2 = new StringBuilder();
			BufferedReader buf2 = new BufferedReader(new InputStreamReader(assets.open("data/leveldata.dat")));
			while ((line = buf2.readLine()) != null){
				str2.append(line);
				str2.append("\n");
			}
			Scanner scan2 = new Scanner(str2.toString());
			
			numOfLang = Integer.parseInt(getValue(scan2));
			
			for(int i=0;i<numOfLang;i++){
				String langCode = getValue(scan2);
				int numOfLevel = Integer.parseInt(getValue(scan2));
				for(int j=0;j<numOfLevel;j++){
					Level tmpLevel = new Level();
					tmpLevel.retrieveData(this, scan2);
					tmpLevel.setLanguageCode(langCode);
					//Log.d("harits", "level name: " + tmpLevel.getLevelName() + " loaded.");
					levels.put(tmpLevel.getLevelName(), tmpLevel);
					
					/* VERSI 1
					String levelName = getValue(scan2);
					Level tmpLevel = new Level(levelName, langCode);
					//Log.d("harits", "level name: " + levelName);

					//get data
					int asteroidDelayTime = Integer.parseInt(getValue(scan2));
					int asteroidSpeed = Integer.parseInt(getValue(scan2));
					int skySpeed = Integer.parseInt(getValue(scan2));
					
					int aksaraCount = Integer.parseInt(getValue(scan2));
					
					for(int k=0;k<aksaraCount;k++){
						tmpLevel.addAksaraName(getValue(scan2));
					}
					
					int monsterCount = Integer.parseInt(getValue(scan2));
					
					for(int k=0;k<monsterCount;k++){
						// Handling input dari file untuk data2 monster dihandle kelas MonsterModel 
						tmpLevel.addMonsterModel(new MonsterModel(scan2));
					}
					
					// Masukin ke model level
					tmpLevel.setAsteroidDelayTime(asteroidDelayTime);
					tmpLevel.setAsteroidSpeed(asteroidSpeed);
					tmpLevel.setSkySpeed(skySpeed);
					
					*/
					//Log.d("harits", "inserting level '" + tmpLevel.getLevelName() + "' to levels");
					
				}
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		/*
		if(languages == null){
			Log.d("harits", "languages is null!");
			return;
		} 
		
		Log.d("harits", "languages size: " + languages.size());
		
		Log.d("harits", "Loaded Asset Data:");
		for(Language entry: languages.values()){
			Log.d("harits", "Language Code: " + entry.getCode());
			for(Aksara aksara: entry.getAllAksara().values()){
				Log.d("harits", "Aksara Name: " + aksara.getName());
				for(CheckPoint cp: aksara.getCheckPoints()){
					Log.d("harits", "Check Point: " + cp.getX() + " " + cp.getY() + " " + cp.getRadius() + " " + cp.getType());
				}
			}
		}
		*/
		
		//Log.d("harits", "Asset data fully loaded.");
		
		/*** 3. Load fonts data ***/
		loadFonts();
	}
	
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
	}

	public void surfaceCreated(SurfaceHolder arg0) {
		//Log.d("harits", "surface created");
		// inisialisasi thread
		initThread();
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {		
		//Log.d("harits", "surface destroyed");
		releaseThread();
		
		//DBLoader.release();
		//bManager.release();
		//sManager.release();
	}
	
	// inisialisasi thread
	public void initThread() {
		if (thread == null || !thread.isAlive()) {
			// jika belom diinisialisasi threadnya atau threadnya sudah tidak hidup lagi, maka
			// instansiasi thread utama
			thread = new GameLoop(getHolder(), this);
			thread.start();
		}
		thread.setRunning(true);
	}
	
	public boolean isThreadRunning(){
		return thread.isRunning();
	}
	
	public void pauseThread(){
		if(thread!=null) 
			thread.setRunning(false);
	}
	
	public void continueThread(){
		if(thread!=null) 
			thread.setRunning(true);
	}
	 
	public void releaseThread() {
		if(thread != null){
			boolean retry = true;
			while (retry) {
				try {
					thread.join();
					retry = false;
					thread = null;
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public void render(Canvas canvas) {
		canvas.setMatrix(matrix);
		canvas.drawColor(Color.BLACK);
		sManager.draw(canvas);
		if(useBlackBar){
			canvas.drawBitmap(blackbar, bbSrc, bbDstL, null);
			canvas.drawBitmap(blackbar, bbSrc, bbDstR, null);
		}
	}

	public void update() {
		//Log.d("harits", "screenmanager update called");
		sManager.update();
	}
	
	public boolean onTouchEvent(MotionEvent event) {	
		//Log.d("harits", "MainGameView keklik lohhh");
		sManager.onTouchEvent(event, magnification, dx, dy);
		return true;
	}
	
	public void dispose(){
		sManager.release();
		bManager.release();
		gameMusic.stop();
		menuMusic.stop();
		gameMusic.dispose();
		menuMusic.dispose();
	}

	public float getRealScreenWidth() {
		return realScreenWidth;
	}


	public float getRealScreenHeight() {
		return realScreenHeight;
	}


	public float getStandardWidth() {
		return standardWidth;
	}


	public float getStandardHeight() {
		return standardHeight;
	}


	public Bitmap getBitmap(String key) {
		return bManager.get(key);
	}
	
	public void initBitmap(String fileName){
		bManager.initBitmap(fileName);
	}
	
	public BitmapManager2 getBitmapManager(){
		return bManager;
	}
	
	/*
	public void initBitmap(String key, int id) throws Exception{
		bManager.put(key, id);
	}
	*/
	
	public void push(IScreen screen){
		sManager.push(screen);
	}
	
	public void pop(){
		sManager.pop();
	}
	
	public ScreenManager getScreenManager(){
		return sManager;
	}
	
	public int getScreenManagerSize(){
		return sManager.getSize();
	}

	public boolean isLastScreen(){
		return sManager.isLastScreen();
	}
	
	public Matrix getMatrix() {
		return matrix;
	}
	
	public float getMagnification(){
		return magnification;
	}
	
	public float getBlackBarWidth(){
		return dx;
	}
	
	public Canvas getCanvas(){
		return thread.getCanvas();
	}

	public void initializeBitmap() {
		bManager.initialize();
		
	}

	public void pauseAudio() {
		if(menuMusic.isPlaying())
			menuMusic.pause();
		if(gameMusic.isPlaying())
			gameMusic.pause();
	}
	
	public void playAudio(){
		if(menuMusic.isPaused()){
			menuMusic.seekBegin();
			menuMusic.play();
		} else if(gameMusic.isPaused()){
			gameMusic.seekBegin();
			gameMusic.play();
		}
	}

	public AssetManager getAssetManager() {
		return assets;
	}

	public void setLang(String lang) {
		globalLang = lang;
	}
	
	public void setLangCode(String lang) {
		langCode = lang;
	}

	public void setUseBlackBar(boolean b) {
		useBlackBar = b;
	}

	public void disposeBitmap(String name) {
		bManager.dispose(name);
	}
}
