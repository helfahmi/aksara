package com.k_tat.aksara.screens;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.objects.Flag;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.system.IScreen;

public class MenuScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private Bitmap rocket_bg;
	private Bitmap astronaut;
	private Bitmap title;
	private BitmapButton play;
	private BitmapButton accuration;
	private BitmapButton setting;
	private BitmapButton quit;
	private BitmapButton flag_gaib;
	private BitmapButton help;
	private Flag flag;
	private Sky sky;
	
	public MenuScreen(MainGameView game) {
		rocket_bg = game.getBitmap("rocket_bg");
		astronaut = game.getBitmap("astronaut");
		title = game.getBitmap("title");

		flag = new Flag(game, game.getGameLang(), 350, 468);
		sky = new Sky(game, "planet", 0, 5);
		
		flag_gaib = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_play_untouched"), game.getBitmap(game.getLangCode() + "_play_touched"), 350, 468, null);
		flag_gaib.setVisible(false);
		flag_gaib.addTouchListener(new TouchListener() {
			
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				// TODO Auto-generated method stub
				game.pop(); 
				game.push(new BahasaScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//init
		this.game = game;
		
		//create button elements indonesia

		help = new BitmapButton(game, game.getBitmap("help_untouched"), game.getBitmap("help_touched"), 400, 350, null);
		help.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.push(new HelpScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		
		play = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_play_untouched"), game.getBitmap(game.getLangCode() + "_play_touched"), 110, 150, null);
		play.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); 
				game.push(new LevelScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		accuration = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_accuracy_untouched"), game.getBitmap(game.getLangCode() + "_accuracy_touched"), 110, 230, null);
		//accuration.setX((float) ((game.getRealScreenWidth() - accuration.getBitmapTouched().getWidth())/2) + 50 );
		accuration.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new AccurationScreen(game, game.getLang()));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		setting = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_setting_untouched"), game.getBitmap(game.getLangCode() + "_setting_touched"), 110, 310, null);
		//setting.setX((float) (game.getRealScreenWidth() - 110 - setting.getBitmapTouched().getWidth())/2);
		setting.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new SettingScreen(game)); 
				game.push(new TransitionScreen(game));

			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		quit = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_quit_untouched"), game.getBitmap(game.getLangCode() + "_quit_touched"), 110, 390, null);
		//quit.setX((float) (game.getRealScreenWidth() - 110 - quit.getBitmapTouched().getWidth())/2);
		quit.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.push(new ConfirmScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(play);
		buttons.add(accuration);
		buttons.add(setting);
		buttons.add(quit);
		buttons.add(flag_gaib);	
		buttons.add(help);
		
		game.playMenuMusic();
	}
		
	@Override
	public void update() {
		flag.update();
		sky.update();
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		sky.draw(canvas, null);
		canvas.drawBitmap(rocket_bg, new Rect(0,0,rocket_bg.getWidth(),rocket_bg.getHeight()), new Rect(0,0,(int)game.getStandardWidth(),(int)game.getStandardHeight()),null);
		canvas.drawBitmap(astronaut, new Rect(0,0,astronaut.getWidth(),astronaut.getHeight()), new Rect(140,465,140+astronaut.getWidth(),465+astronaut.getHeight()),null);
		canvas.drawBitmap(title, new Rect(0,0,title.getWidth(),title.getHeight()), new Rect(120,60,120+title.getWidth(),60+title.getHeight()),null);
		flag.draw(canvas);
		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
	}
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		for(BitmapButton b : buttons){
			if(b.onTouchEvent(e, mag, dx, dy))
				break;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "MenuScreen";
	}
}
