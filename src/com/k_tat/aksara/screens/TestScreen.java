package com.k_tat.aksara.screens;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;

public class TestScreen implements IScreen{
	private MainGameView game;
	private Typeface typeface;
	private Paint paint;
	
	public TestScreen(MainGameView game){
		this.game = game;
		paint = new Paint();
		
		typeface = Typeface.createFromAsset(game.getAssetManager(), "fonts/quarkstorm.ttf");
		/*if(typeface == null)
			Log.d("harits", "typeface is null");*/
		paint.setTypeface(typeface);
		paint.setTextSize(20);
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(Canvas canvas) {
		// TODO Auto-generated method stub
		canvas.drawColor(Color.WHITE);
		canvas.drawText("Test", 0, 0, paint);
	}

	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "TestScreen";
	}

}
