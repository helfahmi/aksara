package com.k_tat.aksara.screens;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Meteor;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.objects.Star;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.system.IScreen;

public class AccurationScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private BitmapButton back;
	private Bitmap rect1, rect2, rect3, rect4, rect5;
	private Bitmap ar, in, ko, en;
	private int ar_acc, in_acc, ko_acc, en_acc;
	private Bitmap ar_satuan, ar_puluhan;
	private Bitmap in_satuan, in_puluhan;
	private Bitmap ko_satuan, ko_puluhan;
	private Bitmap en_satuan, en_puluhan;
	private Bitmap ar_percent, in_percent;
	private String lang;
	private Sky sky;
	private Bitmap title;
	
	public AccurationScreen(MainGameView game, String lang) {		
		//Akurasi ngasal
		ar_acc = 19;
		in_acc = 4;
		ko_acc = 100;
		en_acc = 99;
		
		//translate to bitmap
		if (ar_acc/10 < 1) ar_puluhan = game.getBitmap("accuracy_null"); 
		else ar_puluhan = game.getBitmap("accuracy_"+ar_acc/10);
		
		if (in_acc/10 < 1) in_puluhan = game.getBitmap("accuracy_null"); 
		else in_puluhan = game.getBitmap("accuracy_"+in_acc/10);
		/*
		if (ko_acc/10 < 1) ko_puluhan = game.getBitmap("accuracy_null"); 
		else ko_puluhan = game.getBitmap("accuracy_"+ko_acc/10);
		*/
		if (en_acc/10 < 1) en_puluhan = game.getBitmap("accuracy_null"); 
		else en_puluhan = game.getBitmap("accuracy_"+en_acc/10);
		
		ar_satuan = game.getBitmap("accuracy_"+ar_acc%10);
		in_satuan = game.getBitmap("accuracy_"+in_acc%10);
		//ko_satuan = game.getBitmap("accuracy_"+ko_acc%10);
		//en_satuan = game.getBitmap("accuracy_"+en_acc%10);
		
		
		
		//Create static elements
		title = game.getBitmap(game.getLangCode()+"_accuracy");
		
		//List akurasi
		rect1 = game.getBitmap("accuracy_rectangle");
		rect2 = game.getBitmap("accuracy_rectangle");
		rect3 = game.getBitmap("accuracy_rectangle");
		rect4 = game.getBitmap("accuracy_rectangle");
		ar = game.getBitmap("accuracy_ar");
		in = game.getBitmap("accuracy_in");
		//ko = game.getBitmap("accuracy_ko");
		//en = game.getBitmap("accuracy_en");
		
		ar_percent = game.getBitmap("accuracy_percent");
		in_percent = game.getBitmap("accuracy_percent");
		
		//init
		this.game = game;
		this.lang = lang;
		
		//create button elements	
		back = new BitmapButton(game, game.getBitmap("back_untouched"), game.getBitmap("back_touched"), 31, 23, null);
		
		back.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new MenuScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(back);
		
		sky = new Sky(game, "planet", 0, 5);
	}
		
	@Override
	public void update() {
		sky.update();
	}

	@Override
	public void draw(Canvas canvas) {
		//background
		canvas.drawColor(Color.BLACK);
		sky.draw(canvas, null);
		
		canvas.drawBitmap(title, new Rect(0,0,title.getWidth(),title.getHeight()), new Rect(100,23,100 + title.getWidth(), 23+title.getHeight()),null);
		
		//list akurasi
		canvas.drawBitmap(rect1, new Rect(0,0,rect1.getWidth(),rect1.getHeight()), new Rect(50,120, 50+(int)rect1.getWidth(), 120+(int)rect1.getHeight()),null);
		canvas.drawBitmap(rect2, new Rect(0,0,rect1.getWidth(),rect1.getHeight()), new Rect(50,220, 50+(int)rect1.getWidth(), 220+(int)rect1.getHeight()),null);
		canvas.drawBitmap(rect3, new Rect(0,0,rect1.getWidth(),rect1.getHeight()), new Rect(50,320, 50+(int)rect1.getWidth(), 320+(int)rect1.getHeight()),null);
		canvas.drawBitmap(rect4, new Rect(0,0,rect1.getWidth(),rect1.getHeight()), new Rect(50,420, 50+(int)rect1.getWidth(), 420+(int)rect1.getHeight()),null);
		
		canvas.drawBitmap(ar, new Rect(0,0,ar.getWidth(),ar.getHeight()), new Rect(75,130, 75+(int)ar.getWidth(), 130+(int)ar.getHeight()),null);
		canvas.drawBitmap(in, new Rect(0,0,in.getWidth(),in.getHeight()), new Rect(75,230, 75+(int)in.getWidth(), 230+(int)in.getHeight()),null);
		//canvas.drawBitmap(ko, new Rect(0,0,ko.getWidth(),ko.getHeight()), new Rect(75,330, 75+(int)ko.getWidth(), 330+(int)ko.getHeight()),null);
		//canvas.drawBitmap(en, new Rect(0,0,en.getWidth(),en.getHeight()), new Rect(75,430, 75+(int)en.getWidth(), 430+(int)en.getHeight()),null);
		
		
		//nilai akurasi
		
		//ARAB
		int i = (int)game.getStandardWidth() - 75; //margin kiri
		int j = 145; //margin atas
		int puluhan = (int) ar_puluhan.getWidth(); //lebar bitmaps puluhan
		int satuan = (int) ar_satuan.getWidth(); //lebar bitmaps satuan
		int percent = (int) ar_percent.getWidth(); //lebar bitmaps persen
		
		canvas.drawBitmap(ar_puluhan, new Rect(0,0,ar_puluhan.getWidth(),ar_puluhan.getHeight()), new Rect(i-percent-satuan-puluhan,j,i-percent-satuan,j+ar_puluhan.getHeight()),null);
		canvas.drawBitmap(ar_satuan, new Rect(0,0,ar_satuan.getWidth(),ar_satuan.getHeight()), new Rect(i-percent-satuan,j,i-percent,j+ar_satuan.getHeight()),null);
		canvas.drawBitmap(ar_percent, new Rect(0,0,ar_percent.getWidth(),ar_percent.getHeight()), new Rect(i-percent,j,i,j+ar_satuan.getHeight()),null);
		
		//INDONESIA
		j = 245; //margin atas
		puluhan = (int) in_puluhan.getWidth(); //lebar bitmaps puluhan
		satuan = (int) in_satuan.getWidth(); //lebar bitmaps satuan
		
		canvas.drawBitmap(in_puluhan, new Rect(0,0,in_puluhan.getWidth(),in_puluhan.getHeight()), new Rect(i-percent-satuan-puluhan,j,i-percent-satuan,j+in_puluhan.getHeight()),null);
		canvas.drawBitmap(in_satuan, new Rect(0,0,in_satuan.getWidth(),in_satuan.getHeight()), new Rect(i-percent-satuan,j,i-percent,j+in_satuan.getHeight()),null);
		canvas.drawBitmap(in_percent, new Rect(0,0,in_percent.getWidth(),in_percent.getHeight()), new Rect(i-percent,j,i,j+in_satuan.getHeight()),null);
		
		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
		
	}
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "AccurationScreen";
	}

}
