package com.k_tat.aksara.screens;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.system.IScreen;

public class TutorialScreen implements IScreen{
	private MainGameView game;
	private Bitmap tutorial;
	private int x,y,downTouched;
	private BitmapButton finish;
	
	public TutorialScreen(MainGameView game){
		this.game = game;
		tutorial = game.getBitmap("tutorial");
		//finish = new BitmapButton(game, game.getBitmap(""), bitmapTouched, x, y)
		x = y = 0;
	}
	
	@Override
	public void update() {
			
	}

	@Override
	public void draw(Canvas canvas) {
		//canvas.drawBitmap(tutorial, getX(), getY(), null);
		canvas.drawBitmap(tutorial, new Rect(0, 0, tutorial.getWidth(), tutorial.getHeight()), new Rect(getX(), getY(), getX() + 3629, getY() + 800), null);
	}

	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		final int actioncode = e.getAction() & MotionEvent.ACTION_MASK;	
		
		switch (actioncode) {
			case MotionEvent.ACTION_DOWN:
				downTouched = (int) (e.getX()/mag);
				break;
			case MotionEvent.ACTION_MOVE:
				setX(getX() + (int) (e.getX()/mag - downTouched));
				downTouched = (int) (e.getX()/mag);
				if(getX() <= -3150)
					setX(-3150);
				if(getX() >= 0){
					setX(0);
				}
				break;
			case MotionEvent.ACTION_UP:
				
				break;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String getName() {
		return "TutorialScreen";
		
	}

}
