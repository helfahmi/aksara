package com.k_tat.aksara.screens;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;

public class FontTestScreen implements IScreen{
	private MainGameView game;
	private Typeface loadingFont;
	private Paint fontPaint;
	
	public FontTestScreen(MainGameView game){
		this.game = game;
		loadingFont = Typeface.createFromAsset(game.getAssetManager(), "fonts/BPreplay.otf");
		fontPaint = new Paint();
		fontPaint.setTextSize(40);
		fontPaint.setTypeface(loadingFont);
		fontPaint.setColor(Color.BLACK);
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);
		canvas.drawText("Test YEAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHhh", 0, 200, fontPaint);
	}

	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
