package com.k_tat.aksara.screens;

import java.util.ArrayList;
import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.AccelerometerHandler;
import com.k_tat.aksara.system.BitmapManager;
import com.k_tat.aksara.system.IScreen;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.objects.Animation;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Flag;
import com.k_tat.aksara.objects.Rocket;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;

public class GameOverScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private int counter = 0; //FPS control
	
	private Bitmap fade;
	private Bitmap game_over;
	private BitmapButton retry;
	private BitmapButton quit;
	private BitmapButton mainmenu;
	private String lang;
	
	public GameOverScreen(MainGameView game) {
		//Create static elements
		fade = game.getBitmap("confirm_screen");
		
		//init
		this.game = game;
		this.lang = lang;
		
		//create button elements
		int i = ((int)game.getStandardWidth()-game.getBitmap("in_retry_untouched").getWidth())/2;
		
		//indo
		game_over = game.getBitmap(game.getLangCode() + "_game_over");
		retry = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_retry_untouched"), game.getBitmap(game.getLangCode() + "_retry_touched"), i, 300, null);
		retry.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop();
				game.pop();
				game.push(new GameScreen(game, game.getLevel("Level 1")));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		mainmenu = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_mainmenu_untouched"), game.getBitmap(game.getLangCode() + "_mainmenu_touched"), i, 400, null);
		mainmenu.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); 
				game.pop();
				game.stopGameMusic();
				game.push(new MenuScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		quit = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_quit_untouched"), game.getBitmap(game.getLangCode() + "_quit_touched"), i, 500, null);
		quit.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.push(new ConfirmScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(retry);
		buttons.add(mainmenu);
		buttons.add(quit);
		
	}
		
	@Override
	public void update() {
		
	}

	@Override
	public void draw(Canvas canvas) {
		int i;
		i = ((int)game.getStandardWidth()-game_over.getWidth())/2;
		canvas.drawBitmap(fade, 0, 0, null);
		canvas.drawBitmap(game_over, new Rect(0,0,game_over.getWidth(),game_over.getHeight()), new Rect(i,130, i+(int)game_over.getWidth(), 130+(int)game_over.getHeight()),null);
		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "GameOverScreen";
		
	}
}