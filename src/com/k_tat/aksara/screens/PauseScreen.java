
package com.k_tat.aksara.screens;

import java.util.ArrayList;
import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;

public class PauseScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	
	private Bitmap fade;
	private Bitmap pause;
	private BitmapButton resume;
	private BitmapButton quit;
	private BitmapButton mainmenu;
	
	public PauseScreen(MainGameView game) {
		//Create static elements
		fade = game.getBitmap("fade");
		int i = ((int)game.getStandardWidth()-game.getBitmap(game.getLangCode() + "_play_untouched").getWidth())/2;
		
		//init
		this.game = game;
		
		//create button elements
		// indo
		pause = game.getBitmap(game.getLangCode() + "_pause");
		resume = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_resume_untouched"), game.getBitmap(game.getLangCode() + "_resume_touched"), i, 250, null);
		resume.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); 
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		mainmenu = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_mainmenu_untouched"), game.getBitmap(game.getLangCode() + "_mainmenu_touched"), i, 350, null);
		mainmenu.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.stopGameMusic();
				game.pop(); 
				game.pop();
				game.push(new MenuScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		quit = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_quit_untouched"), game.getBitmap(game.getLangCode() + "_quit_touched"), i, 450, null);
		quit.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.push(new ConfirmScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(resume);
		buttons.add(mainmenu);
		buttons.add(quit);
	}
		
	@Override
	public void update() {
	}

	@Override
	public void draw(Canvas canvas) {
		int i;
		i = ((int)game.getStandardWidth()-pause.getWidth())/2;
		canvas.drawBitmap(fade, new Rect(0,0,fade.getWidth(), fade.getHeight()), new Rect(0,0,480,800), null);
		canvas.drawBitmap(pause, new Rect(0,0,pause.getWidth(),pause.getHeight()), new Rect(i,150, i+(int)pause.getWidth(), 150+(int)pause.getHeight()),null);

		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "PauseScreen";
		
	}
}
