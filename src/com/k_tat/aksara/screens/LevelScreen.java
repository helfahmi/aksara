package com.k_tat.aksara.screens;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.system.IScreen;

public class LevelScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private BitmapButton back;
	private BitmapButton level_button[] = new BitmapButton[20];
	private Bitmap level_number;
	private Bitmap title;
	private Sky sky;
	
	public LevelScreen(MainGameView game) {
		//Create static elements
		title = game.getBitmap(game.getLangCode()+"_level");
		
		//kotak level
		level_number = game.getBitmap("level_1_unfinished");
		
		//init
		this.game = game;
		sky = new Sky(game, "planet", 0, 5);
		
		//create button elements
		
		back = new BitmapButton(game, game.getBitmap("back_untouched"), game.getBitmap("back_touched"), 31, 23, null);
		back.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new MenuScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		float space = (game.getStandardWidth()-4*game.getBitmap("level_square_unfinished").getWidth())/5;
		float sum = space;
		//asumsi belom beres
		
		//game.setcurrLevel(game.getTotalLevel());
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(back);
		int Line_Space = 140;
		
		for(int i = 1;i<=game.getcurrLevel();i++) {
			game.setTmpData(i);
			if ((i == 5) || (i == 9) || (i == 13))  {
				Line_Space = Line_Space + game.getBitmap(i+"_unfinished").getHeight() + 50;
				sum = space;
			}
			if (i == game.getcurrLevel()) {
				level_button[i] = new BitmapButton(game, game.getBitmap(i+"_unfinished"), game.getBitmap(i+"_unfinished"), sum, Line_Space, i);
				
			} else { 
				level_button[i] = new BitmapButton(game, game.getBitmap(i+"_finished"), game.getBitmap(i+"_finished"), sum, Line_Space, i);  
			} 
			level_button[i].addTouchListener(new TouchListener() {
				@Override
				public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
					game.pop();
					game.push(new LoadingScreen(game, game.getLevel(String.valueOf(obj))));
				}
				
				@Override
				public void onTouchMove(MainGameView game, MotionEvent event) {
				}
				
				@Override
				public void onTouchDown(MainGameView game, MotionEvent event) {
				}
			});
			buttons.add(level_button[i]);
			sum = sum+game.getBitmap(i+"_unfinished").getWidth()+space;
		}
	}
		
	@Override
	public void update() {
		sky.update();
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		sky.draw(canvas, null);
		canvas.drawBitmap(level_number, new Rect(0,0,level_number.getWidth(),level_number.getHeight()), new Rect(63,150, 63+(int)level_number.getWidth(), 150+(int)level_number.getHeight()),null);
		canvas.drawBitmap(title, new Rect(0,0,title.getWidth(),title.getHeight()), new Rect(100,23,100 + title.getWidth(), 23+title.getHeight()),null);
		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
		
	}
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public String getName() {
		return "LevelScreen";
		
	}
}
