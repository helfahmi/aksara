
package com.k_tat.aksara.screens;

import java.util.ArrayList;
import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Flag;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;

public class HelpScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	
	private Bitmap fade;
	private Bitmap text;
	private Flag flag;
	
	public HelpScreen(MainGameView game) {
		//init
		this.game = game;
		
		//Create static elements
		fade = game.getBitmap("fade");
		text = game.getBitmap(game.getLangCode()+"_tutorial_msg");
		flag = new Flag(game, game.getGameLang(), 350, 468);
	}
		
	@Override
	public void update() {
		flag.update();
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(fade, 0, 0, null);
		canvas.drawBitmap(text, 20, 450, null);
		flag.draw(canvas);
	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		final int actioncode = e.getAction() & MotionEvent.ACTION_MASK;	
		if(actioncode == MotionEvent.ACTION_UP)
			game.pop();
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public String getName() {
		return "HelpScreen";
		
	}
}
