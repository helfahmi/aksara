package com.k_tat.aksara.screens;

import java.util.ArrayList;

import com.k_tat.aksara.main.MainGameActivity;
import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.models.Level;
import com.k_tat.aksara.objects.Animation;
import com.k_tat.aksara.objects.Asteroid;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Meteor;
import com.k_tat.aksara.objects.Monster;
import com.k_tat.aksara.objects.MonsterManager;
import com.k_tat.aksara.objects.Rocket;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.objects.SpaceStation;
import com.k_tat.aksara.objects.Track;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.system.AccelerometerHandler;
import com.k_tat.aksara.system.GameMusic;
import com.k_tat.aksara.system.GameSound;
import com.k_tat.aksara.system.IScreen;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

public class GameScreen implements IScreen{
	private enum STATE{
		PLAYING, FINISH, DONE;
	};
	private MainGameView game;
	private Asteroid asteroid;
	private Sky sky;
	private Rocket rocket;
	private AccelerometerHandler accelHandler;
	private BitmapButton pause;
	private int current_level = 1;
	private Bitmap game_lvl; //tulisan lvl
	private Bitmap lvl_satuan; //angka satuan level
	private Bitmap lvl_puluhan; //angka puluhan level
	private Bitmap akurasi_puluhan; //angka puluhan akurasi
	private Bitmap akurasi_satuan; // angka satuan akurasi
	private int accuracy;
	private Bitmap game_percent; // tulisan %
	private Track track;
	private MonsterManager monsterManager;
	private int opacity;
	private Paint paint;
	private Bitmap health_bar; 
	private int health;
	private int TakeDamage;
	private int healthIncrease;
	private boolean startCA;
	private Level curLevel;
	private STATE curState;
	private SpaceStation spaceStation;
	private Paint fontPaint;
	private GameSound gameSound;
	
	public void debug(Object obj, String name){
		if(obj == null)
			Log.d("harits", name + " is null");
		else
			Log.d("harits", name + " is not null");
	}
	
	public GameScreen(MainGameView game, Level curLevel) {
		this.game = game;
		this.curLevel = curLevel;
		curState = STATE.PLAYING;
		
		/*** Damage Management ***/
		TakeDamage = 0;
		healthIncrease = 0;
		
		/*** Manage Monster ***/
		monsterManager = new MonsterManager(game, curLevel.getWaves(), this);
		//debug(curLevel, "curLevel");
		//Log.d("harits", "jumlah monster: " + curLevel.getMonsterSize());
		
/*		for(int i=0;i<curLevel.getMonsterSize(curWave);i++){
			//Log.d("harits", "nama monster yg ke-" + i + " adalah " + curLevel.getMonsterName(i));
			//Log.d("harits", "monster delay yg ke-" + i + " adalah " + curLevel.getMonsterDelayTime(i));
			//Log.d("harits", "monster speed yg ke-" + i + " adalah " + curLevel.getMonsterSpeed(i));
			monsterManager.addMonster(new Monster(game, this, curLevel.getMonsterName(i, curWave), curLevel.getMonsterSpeed(i, curWave), curLevel.getMonsterDelayTime(i, curWave)));
		}*/
		
		/*** Manage Track Board ***/
		debug(curLevel, "curLevel");
		//debug(curLevel.getAksaraName(), "curLevel.getAksaraName()");
		track = new Track(game, this, game.getLanguage(curLevel.getLanguageCode()).getAksaras(curLevel.getAksaraNames()));
		
		/** Manage Labels **/
		health_bar = game.getBitmap("health_bar");
		health = health_bar.getWidth();
		/*game_lvl = game.getBitmap("game_lvl");
		lvl_satuan = game.getBitmap("game_" + current_level % 10);
		if (current_level/10 != 0)
			lvl_satuan = game.getBitmap("game_" + current_level/10);*/
		//akurasi_satuan = game.getBitmap("game_9");
		//akurasi_puluhan = game.getBitmap("game_9");
		accuracy = 100;
		fontPaint = game.createFontPaint("quarkstorm", Color.WHITE, 70);
		//game_percent = game.getBitmap("game_percent");
		opacity = 0;
		startCA = false;
		
		pause = new BitmapButton(game, game.getBitmap("pause_untouched"), game.getBitmap("pause_touched"), 387, 23, null);
		
		pause.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.push(new PauseScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});

		/*** Initialization ***/
		spaceStation = new SpaceStation(game);
		asteroid = new Asteroid(game, curLevel.getAsteroidSpeed(), 2, curLevel.getAsteroidDelayTime());
		sky = new Sky(game, "earth", curLevel.getSkySpeed(), 0);
		rocket = new Rocket(game, 240, (int) game.getStandardHeight() - game.getBitmap("rocket").getHeight()/4);
		accelHandler = new AccelerometerHandler(game.getContext());
		paint = new Paint();
		gameSound = game.getAudio().createSound("audio/collision_sound.mp3");
		
		//setelah semua resource loaded, mainin musik
		game.playGameMusic();
	}
	
	public void setAccuracyUI(int acc){
		//Log.d("harits", "Accuracy: " + acc);
		
		accuracy = acc;
/*		akurasi_satuan = game.getBitmap("game_" + String.valueOf(acc % 10));
		akurasi_puluhan = game.getBitmap("game_" + String.valueOf((acc / 10) % 10));*/
	}
	
	public Paint getPaint(){
		return paint;
	}
	
	public Rocket getRocket(){
		return rocket;
	}
	
	public void startCompleteAnimation(){
		startCA = true;
	}
	
	public void TakeDamage (int Damage) {
		
	}
	
	@Override
    public void update() {
            //Log.d("health", "rocket health: " + health);
            sky.update();
            if(curState == STATE.PLAYING){
                    if (health <= 0) {
                            game.pop();
                            game.push(new GameOverScreen(game));
                    } else {
                            if(monsterManager.isCompleted()){
                                    curState = STATE.FINISH;
                            }
                            
                            if (Rect.intersects(asteroid.getRect(), rocket.getRect())) {
                                    //Log.d("health", "damaged");
                            		gameSound.play(0.8f);
                                    TakeDamage = asteroid.getDamage();
                                    asteroid.reset();
                            } 
                            if (TakeDamage > 0) {
                                    health--;
                                    TakeDamage--;
                            }                               
                            
                            if(healthIncrease > 0){
                                    health++;
                                    healthIncrease--;
                            }
                            monsterManager.update();
                    }
            } else if(curState == STATE.FINISH){
            	track.stop();
            	asteroid.stop();
                spaceStation.startMove();
                curState = STATE.DONE;
            } else if(curState == STATE.DONE){
            	if(spaceStation.isCompleted()){
            		sky.stop();
                	rocket.endRocket();
                	if(rocket.isRocketFinished()){
                		game.pop();
                	    game.push(new CompletedScreen(game, Integer.parseInt(curLevel.getLevelName()), accuracy));
                	    if(game.getcurrLevel() == Integer.parseInt(curLevel.getLevelName()))
                	    	game.setcurrLevel(Integer.parseInt(curLevel.getLevelName()) + 1);
                	    game.push(new TransitionScreen(game));
                	}
                }
            }
            track.update();
            asteroid.update();
            spaceStation.update();
            rocket.update(accelHandler.getAccelX());
    }
	
	public void reduceHealth(){
		health--;
	}
	
	public void setTakeDamage(int TD) {
		TakeDamage = TD;
	}
	
	public int getTakeDamage () {
		return TakeDamage;
	}

	@Override
	public void draw(Canvas canvas) {
		if(startCA){
			paint.setColorFilter(new LightingColorFilter(0x009D9D9D, 0));
		}
		
		sky.draw(canvas, paint);
		rocket.draw(canvas, paint);
		spaceStation.draw(canvas, paint);
		
		monsterManager.draw(canvas, paint);
		asteroid.draw(canvas, paint);
		track.draw(canvas, paint);
		pause.draw(canvas);
		//sdssdsdsd
		canvas.drawText(String.valueOf(accuracy) + "%", 36, 72, fontPaint);
		
/*		canvas.drawBitmap(akurasi_puluhan, new Rect(0,0,akurasi_puluhan.getWidth(),akurasi_puluhan.getHeight()), new Rect(36,43, 36+(int)akurasi_puluhan.getWidth(), 43+(int)akurasi_puluhan.getHeight()),null);
		canvas.drawBitmap(akurasi_satuan, new Rect(0,0,akurasi_satuan.getWidth(),akurasi_satuan.getHeight()), new Rect(76,43, 76+(int)akurasi_satuan.getWidth(), 43+(int)akurasi_satuan.getHeight()),null);
		canvas.drawBitmap(game_percent, new Rect(0,0,game_percent.getWidth(),game_percent.getHeight()), new Rect(127,40, 127+(int)game_percent.getWidth(), 40+(int)game_percent.getHeight()),null);
*/		canvas.drawBitmap(health_bar, new Rect(0,0,health,health_bar.getHeight()), new Rect(36,88, 36+health, 88+(int)health_bar.getHeight()),null);
		canvas.drawText("lvl " + curLevel.getLevelName(), 36, 140, fontPaint);
		/*canvas.drawBitmap(game_lvl, new Rect(0,0,game_lvl.getWidth(),game_lvl.getHeight()), new Rect(36,115, 36+(int)game_lvl.getWidth(), 115+(int)game_lvl.getHeight()),null);
		if (current_level/10 != 0) {
			canvas.drawBitmap(lvl_puluhan, new Rect(0,0,lvl_puluhan.getWidth(),lvl_puluhan.getHeight()), new Rect(155,115, 155+(int)lvl_puluhan.getWidth(), 115+(int)lvl_puluhan.getHeight()),null);
			canvas.drawBitmap(lvl_satuan, new Rect(0,0,lvl_satuan.getWidth(),lvl_satuan.getHeight()), new Rect(190,115, 140+(int)lvl_satuan.getWidth(), 190+(int)lvl_satuan.getHeight()),null);
		} else
			canvas.drawBitmap(lvl_satuan, new Rect(0,0,lvl_satuan.getWidth(),lvl_satuan.getHeight()), new Rect(155,115, 155+(int)lvl_satuan.getWidth(), 115+(int)lvl_satuan.getHeight()),null);
*/	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {	
		//cek dulu apakah tombol pause kepencet, kalo iya, jgn proses track. 
		//kalo nggak kepencet, proses track.
		if(!pause.onTouchEvent(e, mag, dx, dy))
			track.onTouchEvent(e, mag, dx, dy);
	}

	public void destroyMonster() {
		monsterManager.explodeAll();
	}
	
	public void bubbleMonster() {
		monsterManager.bubbleAll();
	}

	@Override
	public void dispose() {
		//Log.d("harits", "GameScreen diposed");
		curLevel.dispose(game);
	}

	public void addHealth(int i) {
		health += i;
	}

	@Override
	public String getName() {
		return "GameScreen";
		
	}

	public void setHealthIncrease(int healthIncrease) {
		this.healthIncrease = healthIncrease;
	}
}
