package com.k_tat.aksara.screens;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;

public class TransitionScreen implements IScreen{
	
	private enum STATE{
		TRANSITIONING, FINISHED;
	};
	
	private MainGameView game;
	private Bitmap screen;
	private STATE curState;
	private int curX, curY, speed;
	private Rect src, dst;
	
	public TransitionScreen(MainGameView game){
		this.game = game;
		screen = game.getBitmap("trans");
		curState = STATE.TRANSITIONING;
		curX = 960;
		curY = 0;
		speed = -20; //3pps
		src = new Rect(curX, curY, curX + 480, curY + 800);
		dst = new Rect(0, 0, 480, 800);
	}
	
	@Override
	public void update() {
		if(curState == STATE.TRANSITIONING){
			if(curX > 0){
				curX += speed;	
				src.set(curX, curY, curX + 480, curY + 800);
			} else {
				curState = STATE.FINISHED;
			}
		} else if(curState == STATE.FINISHED){
			game.pop();
		}
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(screen, src, dst, null);
	}

	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "TransitionScreen";
	}
	
	
}
