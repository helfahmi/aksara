package com.k_tat.aksara.screens;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.models.Level;
import com.k_tat.aksara.system.IScreen;
import com.k_tat.aksara.system.Loader;
import com.k_tat.aksara.system.ResourceLoaderThread;

public class LoadingScreen implements IScreen, Loader {
	private MainGameView game;
	private Bitmap loading;
	private Matrix loadingMatrix, loadingMatrix2, loadingMatrix3;
	private Level level;
	private int percent, rotateSpeed, curRotation;
	private boolean isLoadingFinished;
	private ResourceLoaderThread thread;
	private Paint loadingPaint;
	
	public LoadingScreen(MainGameView game, Level level){
		this.game = game;
		this.level = level;
		percent = 0;
		loading = game.getBitmap("galaxy");
		loadingPaint = game.createFontPaint("BPreplay", Color.WHITE, 40);
		loadingMatrix = new Matrix();
		loadingMatrix2 = new Matrix();
		loadingMatrix3 = new Matrix();
		
		loadingMatrix2.setTranslate(0, 400 - loading.getHeight()/2);
		
		curRotation = 0;
		rotateSpeed = -5;
		
		thread = new ResourceLoaderThread(game, level, this);
		
		/*** Enqueue filenames ***/
		//thread.enqueue(fileName);
		thread.enqueue("meteor");
		thread.enqueue("health_bar");
		thread.enqueue("game_lvl");
		thread.enqueue("game_9");
		thread.enqueue("game_percent");
		thread.enqueue("pause_touched");
		thread.enqueue("pause_untouched");
		thread.enqueue("asteroid_enemy3");
		thread.enqueue("rocket");
		thread.enqueue("fade");
		thread.enqueue("in_game_over");
		thread.enqueue("in_retry_untouched");
		thread.enqueue("in_retry_touched");
		thread.enqueue("in_mainmenu_untouched");
		thread.enqueue("in_mainmenu_touched");
		thread.enqueue("in_quit_untouched");
		thread.enqueue("in_quit_touched");
		thread.enqueue("in_pause");
		thread.enqueue("in_play_untouched");
		thread.enqueue("in_resume_untouched");
		
		if(level == null)
			Log.d("harits", "level null");
		else
			Log.d("harits", "level not null");
		
		
		/*for(int i=0;i<level.getMonsterSize();i++){
			thread.enqueueMonster(level.getMonsterName(i));
		}*/
		
		thread.enqueueMonster("monster_blue");
		thread.enqueueMonster("monster_green");
		thread.enqueueMonster("monster_pink");
		thread.enqueueMonster("monster_yellow");
		
		isLoadingFinished = false;
		
		(new Thread(thread)).start();
	}
	
	@Override
	public void update() {
		
		curRotation += rotateSpeed;
		loadingMatrix.setRotate(curRotation, loading.getWidth()/2, 400 - loading.getHeight()/2 + loading.getHeight()/2);
		loadingMatrix3.setConcat(loadingMatrix, loadingMatrix2);
		
		if(isLoadingFinished){
			game.stopMenuMusic();
			game.pop();
			game.push(new GameScreen(game, level));
			game.push(new TransitionScreen(game));
		}
	}
	

	@Override
	public void draw(Canvas canvas) {
		// background
		canvas.drawColor(Color.BLACK);
		canvas.drawBitmap(game.getBitmap("background"), new Rect(0,0,game.getBitmap("background").getWidth(),game.getBitmap("background").getHeight()/2), new Rect(0,0,(int)game.getStandardWidth(),(int)game.getStandardHeight()),null);
		//canvas.drawText("Loading...", 0, 0, fontPaint);
		canvas.drawBitmap(loading, loadingMatrix3, null);
		canvas.drawText(thread.getPercent() + "% Loading...", 5, 790, loadingPaint);
	}

	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLoadingFinished(boolean val) {
		isLoadingFinished = val;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "LoadingScreen";
	}

}
