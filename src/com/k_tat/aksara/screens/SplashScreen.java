package com.k_tat.aksara.screens;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;
import com.k_tat.aksara.system.Loader;
import com.k_tat.aksara.system.ResourceLoaderThread;

public class SplashScreen implements IScreen, Loader{
	
	private MainGameView game;
	
	private long startTime;
	private final long delay = 2000; //3000 milidetik = 3 detik
	private boolean isFinishedLoading;
	
	private Bitmap logo;
	//private Bitmap text;
	
	private ResourceLoaderThread thread;
	private Paint splashPaint1, splashPaint2;
	
	public SplashScreen(MainGameView game) {
        this.game = game;
        startTime = System.currentTimeMillis();
        isFinishedLoading = false;
        
        splashPaint1 = game.createFontPaint("BPreplay", Color.BLACK, 50);
        splashPaint2 = game.createFontPaint("BPreplay", Color.BLACK, 80);
        //load logo
        //text = game.getBitmap("createdby");
        logo = game.getBitmap("logo");
        
        //Kalo nggak butuh ngeload resource buat level, parameter level diisi null aja.
        thread = new ResourceLoaderThread(game, null, this);
        
        //Ngeload asset data (data level, data aksara, dll).
        //Liat fungsinya di MainGameView ( loadAssetsData() ).
        thread.setLoadAssets(true);
        
        thread.enqueue("background");
        thread.enqueue("background2");
        thread.enqueue("planet");
        thread.enqueue("trans");
        
        /*** Level Screen Assets ***/
        
        thread.enqueue("level_1_finished");
        thread.enqueue("level_1_unfinished");
        thread.enqueue("back_touched");
        thread.enqueue("back_untouched");
        thread.enqueue("level_square_unfinished");
        thread.enqueue("level_square_finished");
        
        /*** Accuration Screen Assets ***/
        
        thread.enqueue("accuracy_ar");
        thread.enqueue("accuracy_in");
        for(int i=0;i<9;i++)
        	thread.enqueue("accuracy_" + i);
        thread.enqueue("accuracy_percent");
        thread.enqueue("accuracy_rectangle");
        
        /*** Setting Screen Assets ***/
        
        thread.enqueue("sound");
        thread.enqueue("language");
        thread.enqueue("indonesian_chosen");
        thread.enqueue("indonesian_unchosen");
        thread.enqueue("english_chosen");
        thread.enqueue("english_unchosen");
        thread.enqueue("sound_border");
        thread.enqueue("sound_fill");
        
        /*** Confirm Screen Assets ***/
        
        thread.enqueue("confirm_screen");
        thread.enqueue(game.getLangCode() + "_yes_untouched");
        thread.enqueue(game.getLangCode() + "_areyousure");
        thread.enqueue(game.getLangCode() + "_yes_touched");
        thread.enqueue(game.getLangCode() + "_no_untouched");
        thread.enqueue(game.getLangCode() + "_no_touched");
        
        
        (new Thread(thread)).start();
    }

    @Override
    public void update() {
    	long curTime = System.currentTimeMillis();
    	//Log.d("harits", "time: " + curTime);
    	
    	if((curTime - startTime) >= delay && isFinishedLoading){
    		game.pop();
    		game.setUseBlackBar(true);
            game.push(new MenuScreen(game)); //inisiasi bahasa
            game.push(new TransitionScreen(game));
    	}
    }

    @Override
    public void draw(Canvas canvas) {
    	canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(logo, (game.getStandardWidth() - logo.getWidth())/2, (game.getStandardHeight() - logo.getHeight())/2, null);
        //canvas.drawText("A", 0, 800, testPaint);
        canvas.drawText("Created by", (game.getStandardWidth() - splashPaint1.measureText("Created by"))/2, 160, splashPaint1);
        canvas.drawText("KTat Studio", (game.getStandardWidth() - splashPaint2.measureText("KTat Studio"))/2, 700, splashPaint2);
        //canvas.drawBitmap(text, (game.getStandardWidth() - text.getWidth())/2, (game.getStandardHeight() - logo.getHeight())/2 - 50, null);
    }

    @Override
    public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
            // TODO Auto-generated method stub
            
    }

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	public void setLoadingFinished(boolean b) {
		isFinishedLoading = b;
		
	}

	@Override
	public String getName() {
		return "SplashScreen";
	}
}
