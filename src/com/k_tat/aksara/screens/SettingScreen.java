package com.k_tat.aksara.screens;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioManager;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Meteor;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.objects.Star;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.system.IScreen;

public class SettingScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private BitmapButton back;
	private Bitmap sound;
	private BitmapButton sound_fill;
	private BitmapButton sound_border;
	private Bitmap language;
	private BitmapButton english;
	private BitmapButton indonesia;
	private int volume;
	private AudioManager audioManager;
	private Sky sky;
	private Bitmap title;

	public SettingScreen(MainGameView game) {
		//Create static elements
		audioManager = (AudioManager) game.getContext().getSystemService(Context.AUDIO_SERVICE);
		//volume = (int) ((float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)/audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * game.getBitmap("sound_fill").getWidth());
		volume = game.getcurrVolume();
		sound = game.getBitmap("sound");
		language = game.getBitmap("language");
		title = game.getBitmap(game.getLangCode()+"_settings");
		
		//init
		this.game = game;
		sky = new Sky(game, "planet", 0, 5);
		
		//create button elements	
		back = new BitmapButton(game, game.getBitmap("back_untouched"), game.getBitmap("back_touched"), 31, 23, null);
		back.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new MenuScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
				
		sound_fill = new BitmapButton(game, game.getBitmap("sound_fill"), game.getBitmap("sound_fill"), 135, 210, null);
		sound_fill.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				if (event.getX() / game.getMagnification() < 200) {
					volume = 0;
					game.setcurrVolume(volume);
				} else if (event.getX() / game.getMagnification() > 135 + sound_fill.getBitmapTouched().getWidth() - 25) {
					volume = sound_fill.getBitmapTouched().getWidth();
					game.setcurrVolume(volume);
				}
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
				volume = (int) (event.getX() / game.getMagnification() - 135);
				Log.d("RENUSA", volume+"(VolBar)");
				game.setcurrVolume(volume);
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
				volume = (int) (event.getX() / game.getMagnification() - 135);
				game.setcurrVolume(volume);
				Log.d("posisi touch : ", "" + (int) (event.getX() / game.getMagnification()));
			}
		});
		
		sound_border = new BitmapButton(game, game.getBitmap("sound_border"), game.getBitmap("sound_border"), 135, 210, null);
		sound_border.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				//setting volume
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		english = new BitmapButton(game, game.getBitmap("english_unchosen"), game.getBitmap("english_chosen"), 140, 400, null);
		english.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.setLang("english");
				game.setLangCode("en");
				game.push(new MenuScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		indonesia = new BitmapButton(game, game.getBitmap("indonesian_unchosen"), game.getBitmap("indonesian_chosen"), 140, 315, null);
		indonesia.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.setLang("indonesia");
				game.setLangCode("in");
				game.push(new MenuScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
				
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(back);
		buttons.add(sound_fill);
		buttons.add(sound_border);
		buttons.add(english);
		buttons.add(indonesia);
		
	}
		
	@Override
	public void update() {
		sound_fill.setWidth(volume);
		if (game.getLang().equals("indonesia")) {
			indonesia.setBitmapUntouched(game.getBitmap("indonesian_chosen"));
			english.setBitmapUntouched(game.getBitmap("english_unchosen"));
		} else {
			indonesia.setBitmapUntouched(game.getBitmap("indonesian_unchosen"));
			english.setBitmapUntouched(game.getBitmap("english_chosen"));
		}
		sky.update();
	}

	@Override
	public void draw(Canvas canvas) {
		//background
		canvas.drawColor(Color.BLACK);
		sky.draw(canvas, null);
		canvas.drawBitmap(title, new Rect(0,0,title.getWidth(),title.getHeight()), new Rect(100,23,100 + title.getWidth(), 23+title.getHeight()),null);
		canvas.drawBitmap(sound, new Rect(0,0,sound.getWidth(),sound.getHeight()), new Rect(50,180,50+(int)sound.getWidth(),180+(int)sound.getHeight()), null);
		canvas.drawBitmap(language, new Rect(0,0,language.getWidth(),language.getHeight()), new Rect(50,307,50+(int)language.getWidth(),307+(int)language.getHeight()), null);

		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
		
	}
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getName() {
		return "SettingScreen";
	}
}
