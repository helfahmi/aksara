package com.k_tat.aksara.screens;

import java.util.ArrayList;
import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.AccelerometerHandler;
import com.k_tat.aksara.system.BitmapManager;
import com.k_tat.aksara.system.IScreen;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.objects.Animation;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.Flag;
import com.k_tat.aksara.objects.Rocket;
import com.k_tat.aksara.objects.Sky;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;

public class CompletedScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private Sky sky;
	private Bitmap level_completed;
	private Bitmap kapal;
	private Bitmap accuracy;
	private int akurasi;
	private Bitmap mode;
	private Bitmap level;
	private Bitmap satuan, puluhan, persen;
	private Animation earth;
	private int counter = 0;
	private String lang;
	private Paint paint1, paint2;
	public CompletedScreen(MainGameView game, int lvl, int acc) {
		//Create static elements
		sky = new Sky(game, "earth", 0, 5);
		level_completed = game.getBitmap("level_completed");
		kapal = game.getBitmap("space_station");
		accuracy = game.getBitmap(game.getLangCode()+"_accuracy");
		akurasi = acc; //masih ngasal
		paint1 = game.createFontPaint("quarkstorm.ttf", Color.rgb(127, 201, 238), 30);
		paint2 = game.createFontPaint("quarkstorm.ttf", Color.rgb(123, 196, 160), 20);
		//translate to bitmap
		/*if (akurasi/10 < 1) 
			puluhan = game.getBitmap("accuracy_null"); 
		else 
			puluhan = game.getBitmap("accuracy_"+akurasi/10);

		satuan = game.getBitmap("accuracy_"+akurasi%10);
		persen = game.getBitmap("accuracy_percent");*/
		
		mode = game.getBitmap("accuracy_"+game.getGameLang());
		level = game.getBitmap(lvl+"_finished");
				
		//init
		this.game = game;
		this.lang = lang;
		
		/*
		//create button elements
		int i = ((int)game.getStandardWidth()-game.getBitmap("in_retry_untouched").getWidth())/2;
		
		
		retry = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_retry_untouched"), game.getBitmap(game.getLangCode() + "_retry_touched"), i, 300);
		retry.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event) {
				game.pop();
				game.pop();
				game.push(new GameScreen(game, game.getLevel("Level 1")));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		*/
		
		
		buttons = new ArrayList<BitmapButton>();
	}
		
	@Override
	public void update() {
		sky.update();
		
	}

	@Override
	public void draw(Canvas canvas) {
		sky.draw(canvas, null);
		/*//int i = (int) (game.getStandardWidth() - accuracy.getWidth() - satuan.getWidth() - puluhan.getWidth() - persen.getWidth() - 35)/2;
		canvas.drawBitmap(accuracy, i, 570, null);
		i = i+accuracy.getWidth()+25;
		canvas.drawText("akurasi", i, 570, paint1);
		//canvas.drawBitmap(puluhan, i, 580, null);
		i = i+satuan.getWidth()+5;
		canvas.drawText(akurasi + "%", i, 580, paint2);
		//canvas.drawBitmap(satuan, i, 580, null);
		i = i+puluhan.getWidth()+5;
		//canvas.drawBitmap(persen, i, 570, null);
		
		i = (int) (game.getStandardWidth() - level_completed.getWidth())/2; 
		canvas.drawBitmap(level_completed, i, 400, null);
		
		i = (int) (game.getStandardWidth() - kapal.getWidth())/2;
		//canvas.drawBitmap(kapal, i, 130,null);
		
		i = (int) (game.getStandardWidth() - mode.getWidth() - level.getWidth() - 25)/2;
		canvas.drawBitmap(mode, i, 220, null);
		canvas.drawBitmap(level, i+mode.getWidth()+25, 220, null);*/
		
		
		//for(BitmapButton b : buttons){
			//b.draw(canvas);
		//}
	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		final int actioncode = e.getAction() & MotionEvent.ACTION_MASK;	
		if(actioncode == MotionEvent.ACTION_UP){
			game.pop();
			game.push(new LevelScreen(game));
			game.stopGameMusic();
			game.playMenuMusic();
			game.push(new TransitionScreen(game));
		}
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public String getName() {
		return "CompletedScreen";
		
	}
	
}