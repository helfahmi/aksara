package com.k_tat.aksara.screens;

import java.util.ArrayList;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.IScreen;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;

public class ConfirmScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	
	private Bitmap confirm_screen;
	private Bitmap confirm_text; //r u sure?
	private BitmapButton yes;
	private BitmapButton no;
	
	public ConfirmScreen(MainGameView game) {
		//Create static elements
		confirm_screen = game.getBitmap("confirm_screen");
		int i = ((int)game.getStandardWidth()-game.getBitmap(game.getLangCode() + "_yes_untouched").getWidth())/2;
		
		//init
		this.game = game;
		
		//create button elements
		// indo
		confirm_text = game.getBitmap(game.getLangCode() + "_areyousure");
		
		yes = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_yes_untouched"), game.getBitmap(game.getLangCode() + "_yes_touched"), i, 250, null);
		yes.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop();
				game.pop();
				game.pop();
				game.finish();
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		no = new BitmapButton(game, game.getBitmap(game.getLangCode() + "_no_untouched"), game.getBitmap(game.getLangCode() + "_no_touched"), i, 350, null);
		no.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop();
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(yes);
		buttons.add(no);
	}
		
	@Override
	public void update() {
	}

	@Override
	public void draw(Canvas canvas) {
		int i;
		i = ((int)game.getStandardWidth()-confirm_text.getWidth())/2;
		canvas.drawBitmap(confirm_screen, new Rect(0,0,confirm_screen.getWidth(), confirm_screen.getHeight()), new Rect(0,0,480,800), null);
		canvas.drawBitmap(confirm_text, new Rect(0,0,confirm_text.getWidth(),confirm_text.getHeight()), new Rect(i,150, i+(int)confirm_text.getWidth(), 150+(int)confirm_text.getHeight()),null);

		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
	}
	
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		// TODO Auto-generated method stub
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "ConfirmScreen";
		
	}
}
