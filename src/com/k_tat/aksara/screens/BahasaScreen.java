package com.k_tat.aksara.screens;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.system.IScreen;

public class BahasaScreen implements IScreen{
	private ArrayList<BitmapButton> buttons;
	private MainGameView game;
	private Bitmap menu;
	private BitmapButton back;
	private BitmapButton alfabet, hijaiyah, hangeul, hiragana;
	private Bitmap title;
	
	public BahasaScreen(MainGameView game) {		
		
		//Create static elements
		menu = game.getBitmap("menu");
		title = game.getBitmap(game.getLangCode()+"_mode");
		float space = (game.getStandardWidth() - game.getBitmap("alfabet_untouched").getWidth())/2;
		//List kotak bahasa
		alfabet = new BitmapButton(game, game.getBitmap("alfabet_untouched"), game.getBitmap("alfabet_touched"), space, 120, null);
		alfabet.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.setGameLangCode("in");
				game.pop(); //
				game.push(new MenuScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		hijaiyah = new BitmapButton(game, game.getBitmap("hijaiyah_untouched"), game.getBitmap("hijaiyah_touched"), space, 230, null);
		hijaiyah.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.setGameLangCode("ar");
				game.pop(); //
				game.push(new MenuScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		hangeul = new BitmapButton(game, game.getBitmap("hangeul_untouched"), game.getBitmap("hangeul_touched"), space, 340, null);
		hangeul.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		hiragana = new BitmapButton(game, game.getBitmap("hiragana_untouched"), game.getBitmap("hiragana_touched"), space, 450, null);
		hiragana.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		//init
		this.game = game;
		
		//create button elements	
		back = new BitmapButton(game, game.getBitmap("back_untouched"), game.getBitmap("back_touched"), 31, 23, null);
		
		back.addTouchListener(new TouchListener() {
			@Override
			public void onTouchUp(MainGameView game, MotionEvent event, Object obj) {
				game.pop(); //
				game.push(new MenuScreen(game));
				game.push(new TransitionScreen(game));
			}
			
			@Override
			public void onTouchMove(MainGameView game, MotionEvent event) {
			}
			
			@Override
			public void onTouchDown(MainGameView game, MotionEvent event) {
			}
		});
		
		buttons = new ArrayList<BitmapButton>();
		buttons.add(back);
		buttons.add(alfabet);
		buttons.add(hijaiyah);
		buttons.add(hangeul);
		buttons.add(hiragana);
	}
		
	@Override
	public void update() {
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(menu, new Rect(0,0,menu.getWidth(),menu.getHeight()), new Rect(0,0, (int)game.getStandardWidth(), (int)game.getStandardHeight()),null); //background
		canvas.drawBitmap(title, new Rect(0,0,title.getWidth(),title.getHeight()), new Rect(100,23,100 + title.getWidth(), 23+title.getHeight()),null);
		for(BitmapButton b : buttons){
			b.draw(canvas);
		}
		
	}
	@Override
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		for(BitmapButton b : buttons){
			b.onTouchEvent(e, mag, dx, dy);
		}
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public String getName() {
		return "BahasaScreen";
	}

}