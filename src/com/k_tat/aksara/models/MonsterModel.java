package com.k_tat.aksara.models;

import java.util.Scanner;

public class MonsterModel {
	private int delayTime;
	private int speed;
	private String name;
	
	public MonsterModel(){
	}
	
	public void retrieveData(Scanner scan){
		name = getValue(scan);
		delayTime = Integer.parseInt(getValue(scan));
		speed = Integer.parseInt(getValue(scan));
	}
	
	public String getValue(Scanner scan){
	/* Get a line of string that is not a comment in the asset file */
		String tmpLine;
		do {
			tmpLine = scan.nextLine();
		} while(tmpLine.charAt(0) == '#');
		return tmpLine;
	}

	public int getDelayTime() {
		return delayTime;
	}

	public int getSpeed() {
		return speed;
	}

	public String getName() {
		return name;
	}
}
