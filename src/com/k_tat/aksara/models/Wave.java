package com.k_tat.aksara.models;

import java.util.ArrayList;
import java.util.Scanner;

import com.k_tat.aksara.main.MainGameView;

public class Wave {
	private ArrayList<MonsterModel> models;
	
	public Wave(){
		models = new ArrayList<MonsterModel>();
	}
	
	public ArrayList<MonsterModel> getMonsters(){
		return models;
	}
	
	public MonsterModel getMonster(int i){
		return models.get(i);
	}
	
	public void retrieveData(MainGameView game, Scanner scan){
		int nMonster = Integer.parseInt(game.getValue(scan));
		for(int i=0;i<nMonster;i++){
			MonsterModel tmp = new MonsterModel();
			tmp.retrieveData(scan);
			models.add(tmp);
		}
	}
	
	public int getSize(){
		return models.size();
	}
}
