package com.k_tat.aksara.models;

import java.util.ArrayList;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.GameMusic;

import android.util.Log;

public class Aksara {
	private String name, realName;
	private ArrayList<CheckPoint> checkpoints;
	private int currentCheckPointTarget;
	private GameMusic sound;
	private String soundResName;
	
	public Aksara(String name, String soundResName){
		currentCheckPointTarget = 0; //belum kena check point manapun
		checkpoints = new ArrayList<CheckPoint>();
		this.name = name;
		this.sound = null;
		this.soundResName = soundResName;
	}
	
	public Aksara(String name, GameMusic sound){
		currentCheckPointTarget = 0; //belum kena check point manapun
		checkpoints = new ArrayList<CheckPoint>();
		this.name = name;
		this.sound = sound;
		this.soundResName = null;
	}
	
	public void initSound(MainGameView game){
		if(sound == null){
			//Log.d("harits", "nama filenya: " + soundResName);
			sound = game.getAudio().createMusic(soundResName);
		}
	}
	
	public GameMusic getSound(){
		return sound;
	}
	
	public void reset(){
		currentCheckPointTarget = 0; //belum kena check point manapun
	}
	
	public ArrayList<CheckPoint> getCheckPoints(){
		return checkpoints;
	}
	
	public CheckPoint getCurrentCheckPoint(){
		return checkpoints.get(currentCheckPointTarget);
		
	}
	
	public int getCurrentCheckPointIndex(){
		return currentCheckPointTarget;
	}
	
	public boolean isOnCheckPoint(int x, int y, String cpType){
		try {
			CheckPoint cp = checkpoints.get(currentCheckPointTarget);
			//Log.d("harits", "masuk '"+cp.getType() + "'");
			
			int deltaX = x - cp.getX();
			int deltaY = y - cp.getY();
			
			//if(cp.getType().equals("DOWN"))
				//Log.d("harits", "diff: " + (deltaX*deltaX + deltaY*deltaY) + " < " + (cp.getRadius()*cp.getRadius()));
			
			if(cp.getType().equals(cpType)){
				if(deltaX*deltaX + deltaY*deltaY < cp.getRadius()*cp.getRadius()){
				/* kalo si titik parameter berada dalam radius checkpoint */
					return true;
				}
			}
			return false;
		} catch (Exception err){
			return false;
		}
	}	
	
	public boolean isFinished(){
		return (currentCheckPointTarget == checkpoints.size());
	}
	
	public void nextCheckPoint(){
		if(currentCheckPointTarget > checkpoints.size())
			currentCheckPointTarget = checkpoints.size();
		else
			currentCheckPointTarget++;
		
		//Log.d("harits", "index sekarang: " + getCurrentCheckPointIndex());
	}
	
	public void prevCheckPoint(){
		if(currentCheckPointTarget < 0)
			currentCheckPointTarget = 0;
		else
			currentCheckPointTarget--;
	}
	
	public String getName(){
		return name;
	}
	
	public String getRealName(){
		return realName;
	}
	
	public void setRealName(String name){
		realName = name;
	}
	
	public void addCheckPoint(CheckPoint cp){
		checkpoints.add(cp);
	}
	
	public void dispose(){
		//Log.d("harits", "sound disposed");
		if(sound != null){
			sound.dispose();
			sound = null;
		}
	}
}
