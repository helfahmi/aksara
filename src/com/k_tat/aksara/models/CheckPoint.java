package com.k_tat.aksara.models;

public class CheckPoint{
	private int x, y, r;
	private String type;
	
	public CheckPoint(int x, int y, int r, String type){
		this.x = x;
		this.y = y;
		this.r = r;
		this.type = type;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public int getRadius(){
		return r;
	}
	
	public String getType(){
		return type;
	}
}