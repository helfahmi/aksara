package com.k_tat.aksara.models;

import java.util.ArrayList;
import java.util.Scanner;

import android.util.Log;

import com.k_tat.aksara.main.MainGameView;

public class Level {
    private int asteroidDelayTime, monsterDelayTime, skySpeed, monsterSpeed, asteroidSpeed, monsterSize;
    private String languageCode, levelName;
    private ArrayList<String> aksaraNames;
    private ArrayList<Wave> waves;
    
    public Level(){
    	waves = new ArrayList<Wave>();
        aksaraNames = new ArrayList<String>();
    }
    
    public Level(String levelName, String langCode){
        //Log.d("harits", "level created. level name: " + levelName);
        this.levelName = levelName;
        this.languageCode = langCode;
        waves = new ArrayList<Wave>();
        aksaraNames = new ArrayList<String>();
    }
    
    public String getMonsterName(int i, int wave){
        //ambil nama sprite monster ke i
        return waves.get(wave).getMonster(i).getName();
    }
    
    public int getMonsterDelayTime(int i, int wave){
        //ambil delay sprite monster ke i
        return waves.get(wave).getMonster(i).getDelayTime();
    }
    
    public int getMonsterSpeed(int i, int wave){
        //ambil speed sprite monster ke i
        return waves.get(wave).getMonster(i).getSpeed();
    }

    public int getAsteroidDelayTime() {
        return asteroidDelayTime;
    }

    public void setAsteroidDelayTime(int asteroidDelayTime) {
        this.asteroidDelayTime = asteroidDelayTime;
    }
    
    public void setMonsterDelayTime(int monsterDelayTime) {
        this.monsterDelayTime = monsterDelayTime;
    }

    public int getSkySpeed() {
        return skySpeed;
    }

    public void setSkySpeed(int skySpeed) {
        this.skySpeed = skySpeed;
    }
    
    public void setMonsterSpeed(int monsterSpeed) {
        this.monsterSpeed = monsterSpeed;
    }

    public int getAsteroidSpeed() {
        return asteroidSpeed;
    }

    public void setAsteroidSpeed(int asteroidSpeed) {
        this.asteroidSpeed = asteroidSpeed;
    }

    public int getMonsterSize(int wave) {
        return waves.get(wave).getSize();
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getAksaraName(int i) {
       return aksaraNames.get(i);
    }
    
    public void addAksaraName(String name){
    	aksaraNames.add(name);
    }

    public String getLevelName() {
        return levelName;
    }
    
    public ArrayList<Wave> getWaves(){
    	return waves;
    }

	public ArrayList<String> getAksaraNames() {
		return aksaraNames;
	}
	
	public void retrieveData(MainGameView game, Scanner scan2){
		this.levelName = game.getValue(scan2);
		//jksjdkjs
		//this.languageCode = langCode;
		//Level tmpLevel = new Level(levelName, langCode);
		Log.d("harits", "level name: " + levelName);

		//get data
		this.asteroidDelayTime = Integer.parseInt(game.getValue(scan2));
		this.asteroidSpeed = Integer.parseInt(game.getValue(scan2));
		this.skySpeed = Integer.parseInt(game.getValue(scan2));
		
		int aksaraCount = Integer.parseInt(game.getValue(scan2));
		
		for(int k=0;k<aksaraCount;k++){
			this.addAksaraName(game.getValue(scan2));
		}
		
		//monster data starts here
		/*
		int monsterCount = Integer.parseInt(game.getValue(scan2));
		
		for(int k=0;k<monsterCount;k++){
			// Handling input dari file untuk data2 monster dihandle kelas MonsterModel 
			MonsterModel tmpModel = new MonsterModel();
			tmpModel.retrieveData(scan2);
			this.addMonsterModel(tmpModel);
		}
		*/
		int nWave = Integer.parseInt(game.getValue(scan2));
		for(int i=0;i<nWave;i++){
			Wave tmp = new Wave();
			tmp.retrieveData(game, scan2);
			waves.add(tmp);
		}
	}
	
	public void load(MainGameView game){
		
		/*if(game.getLanguage(getLanguageCode()).getAksaras(getAksaraNames()) == null){
			Log.d("harits", "aksaras null");
		} else
			Log.d("harits", "aksaras not null");*/
		for(String name: getAksaraNames()){
			game.initBitmap(name);
		}
		
		for(Aksara aksara: game.getLanguage(getLanguageCode()).getAksaras(getAksaraNames())){
			aksara.initSound(game);
			/*if(aksara.getSound() == null)
				Log.d("harits", "soundnya masih null nih");
			else
				Log.d("harits", "soundnya udah keload");*/
		}
	}
	
	public void dispose(MainGameView game){
		//dispose data aksara: resource sound
		for(Aksara aksara: game.getLanguage(getLanguageCode()).getAksaras(getAksaraNames())){
			aksara.dispose();
		}
		
		for(String name: getAksaraNames()){
			game.disposeBitmap(name);
		}
	}
}
