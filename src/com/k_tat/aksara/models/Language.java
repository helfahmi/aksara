package com.k_tat.aksara.models;

import java.util.ArrayList;
import java.util.HashMap;


public class Language {
	private String langcode; //AR EN dst
	private HashMap<String, Aksara> aksaras;
	
	public Language(String langcode){
		aksaras = new HashMap<String, Aksara>();
		this.langcode = langcode;
	}
	
	public String getCode(){
		return langcode;
	}
	
	public HashMap<String, Aksara> getAllAksara(){
		return aksaras;
	}
	
	public Aksara getAksara(String name){
		return aksaras.get(name);
	}
	
	public void addAksara(Aksara a){
		aksaras.put(a.getName(), a);
	}

	public ArrayList<Aksara> getAksaras(ArrayList<String> aksaraNames) {
		ArrayList<Aksara> temp = new ArrayList<Aksara>();
		for(String s: aksaraNames){
			temp.add(getAksara(s));
		}
		return temp;
	}
}
