package com.k_tat.aksara.objects;

import java.util.ArrayList;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.models.MonsterModel;
import com.k_tat.aksara.models.Wave;
import com.k_tat.aksara.screens.GameScreen;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.screens.GameScreen;
import com.k_tat.aksara.system.GameSound;

public class MonsterManager {
	private MainGameView game;
	private GameScreen parent;
	private ArrayList<Monster> monsters;
	private ArrayList<Wave> waves;
	private int curWave;
	private boolean isCompleted;
	private GameSound gameSound;
	
	public MonsterManager(MainGameView game, ArrayList<Wave> waves, GameScreen parent){
		this.game = game;
		this.parent = parent;
		this.waves = waves;
		monsters = new ArrayList<Monster>();
		curWave = 0;
		isCompleted = false;
		initWave(curWave);
		gameSound = game.getAudio().createSound("audio/collision_sound.mp3");
	}
	
	public MonsterManager(MainGameView game, GameScreen parent){
		this.game = game;
		this.parent = parent;
		monsters = new ArrayList<Monster>();
		gameSound = game.getAudio().createSound("audio/collision_sound.mp3");
	}
	
	public void initWave(int wave){
		monsters.clear();
		for(MonsterModel mm: waves.get(wave).getMonsters()){
			monsters.add(new Monster(game, parent, mm.getName(), mm.getSpeed(), mm.getDelayTime()));
		}
	}
	
	public void addMonster(Monster m){
		monsters.add(m);
	}
	
	public void explodeAll(){
		for(Monster m:monsters){
			m.explode();
		}
	}
	
	public boolean isCompleted(){
		return isCompleted;
	}
	
	public void update(){
		boolean isFinished = true;
		//Log.d("harits", "Jumlah monster: " + monsters.size());
		for(Monster m: monsters){
			if(!m.isFinished())
				isFinished = false;
			else
				continue;
			
			//Log.d("harits", "not finished.");
			if(Rect.intersects(parent.getRocket().getRect(), m.getRect())){
				if(m.isBubble()){
					
					//Log.d("harits", "bubble hit taken.");
					parent.setHealthIncrease(10);
					m.reset();
					//m.finish();
					Log.d("harits", "is this Monster finished? " + m.isFinished());
				} else if(!m.isExploding()){
					Log.d("harits", "damage taken.");
					parent.setTakeDamage(50);
					gameSound.play(0.8f);
					m.explode(); //explode() udah sekalian manggil finish()
				}
			}
			m.update();
		}
		
		if(isFinished){
			Log.d("harits", "Wave " + (curWave + 1) + " finished.");
			curWave++;
			if(curWave < waves.size()){
				Log.d("harits", "Continue to Wave " + (curWave + 1) + ".");
				initWave(curWave);
			} else {
				isCompleted = true;
				Log.d("harits", "All Waves completed.");
			}
		}
	}
	
	public void draw(Canvas canvas, Paint paint){
		for(Monster m: monsters){
			m.draw(canvas, paint);
		}
	}

	public void bubbleAll() {
		for(Monster m:monsters){
			m.beBubble();
		}
		
	}
}
