package com.k_tat.aksara.objects;

import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.Touchable;

public class Boogey implements Touchable {
	private int counter;
	private Animation boogey;
	private MainGameView game;
	private TouchListener action;
	private boolean touched = false;
	private Rect rect;
	private int speed; //sumbu-y
	private Random random;
	private boolean isVisible;
	private float x, y;
	
	public interface TouchListener {
		public void onTouchDown(MainGameView game, MotionEvent event);
		public void onTouchUp(MainGameView game, MotionEvent event);
		public void onTouchMove(MainGameView game, MotionEvent event);
	}
	
	public Boogey(MainGameView game, int x, int y){
		boogey = new Animation(game.getBitmap("shadow"), x, y, 1, 2);
		this.game = game;
		counter = 0;
	}
	
	public float getX(){
		return boogey.getX();
	}
	
	public float getY(){
		return boogey.getY();
	}
	
	public void draw(Canvas canvas) {
		boogey.draw(canvas, null);
	}
	
	public void update(){
		if (counter == game.getFPS()/6) {
			boogey.update();
			counter = 0;
		}
		else 
			counter++;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event, float mag, float dx, float dy) {
		final int actioncode = event.getAction() & MotionEvent.ACTION_MASK;	
		
		RectF r = new RectF((x + dx)*mag, y*mag, (x + dx + boogey.getBitmap().getWidth())*mag,  (y + boogey.getBitmap().getHeight())*mag);
		
		if (r.contains(event.getX(), event.getY())){
			switch (actioncode) {
				case MotionEvent.ACTION_DOWN:
					//Log.d("harits", "masuk action down");
					action.onTouchDown(game,event);
					touched = true;
					break;
				case MotionEvent.ACTION_MOVE:
					//Log.d("harits", "masuk action move");
					action.onTouchMove(game,event);
					break;
				case MotionEvent.ACTION_UP:
					//Log.d("harits", "masuk action up");
					action.onTouchUp(game,event);
					touched = false;
					break;		
			}
			return true;
		}else{
			if(touched){
				switch (actioncode) {
				case MotionEvent.ACTION_MOVE:
					action.onTouchUp(game,event);
					touched = false;
					break;
				case MotionEvent.ACTION_UP:
					action.onTouchUp(game,event);
					touched = false;
					break;		
				}
			}
			return false;
		}
	}
}
