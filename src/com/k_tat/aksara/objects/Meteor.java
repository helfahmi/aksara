package com.k_tat.aksara.objects;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.screens.GameScreen;

public class Meteor {
	private MainGameView game;
	private Random random;
	private int speed;
	private float curX, curY, curR;
	private Animation meteor;
	private int delayTime;
	private boolean isVisible;
	private GameScreen parent;
	private Matrix mtxMove, mtxRotate, mtx;
	
	public Meteor(MainGameView game, Bitmap img, int spd){
		this.game = game;
		random = new Random();
		mtxMove = new Matrix();
		mtxRotate = new Matrix();
		mtx = new Matrix();
		meteor = new Animation(img, 0, 0, 1, 4);
		reset();
		speed = spd;
		isVisible = false;
	}
	
	public void reset() {
		curX = (int) (game.getStandardWidth() + meteor.getWidth());
		curY = random.nextInt((int) game.getStandardHeight() - meteor.getHeight());
		curR = random.nextInt(90) - 45;
		mtxRotate.setRotate(curR, meteor.getWidth()/3, meteor.getHeight()/2);
		delayTime = 500; //500 frame maksudnya, kira 60 FPS, berarti kira2 500/60 = ~8s
	}
	
	public void update(){
		if(delayTime > 0){
			delayTime--;
			return;
		} else
			isVisible = true;
			
		if(curX <= -meteor.getWidth()){
			reset();
		}
		
		mtxMove.setTranslate(curX, curY);
		mtx.setConcat(mtxMove, mtxRotate);
		curX -= (float) speed * Math.cos((double) curR/180 * Math.PI);
		curY -= (float) speed * Math.sin((double) curR/180 * Math.PI);
		meteor.update();
	}
	
	public void draw(Canvas canvas, Paint paint){
		if(isVisible)
			meteor.draw(canvas, mtx, paint);
	}
	
	public void onTouchEvent(MotionEvent e, float mag, float dx) {	
		//cek dulu apakah tombol pause kepencet, kalo iya, jgn proses track. 
		//kalo nggak kepencet, proses track.
		double x = e.getX()/mag + dx;
		double y = e.getY()/mag;
		if(x >= curX && x <= (curX + meteor.getWidth()) && y >= curY && y <= (curY + meteor.getHeight())){
			parent.addHealth(20);
		}
	}
}
