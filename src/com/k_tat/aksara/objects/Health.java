package com.k_tat.aksara.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.k_tat.aksara.main.MainGameView;

public class Health {
	private Bitmap health_bar; 
	private int health;
	private MainGameView game;
	
	public Health (MainGameView game) {
		this.game = game;
		health_bar = game.getBitmap("health_bar");
		health = health_bar.getWidth();
	}
	
	public int getHealth() {
		return health;
	}
	
	public void draw(Canvas canvas) {
		canvas.drawBitmap(health_bar, new Rect(0,0,health,health_bar.getHeight()), new Rect(36,88, 36+health, 88+(int)health_bar.getHeight()),null);
	}
}
