package com.k_tat.aksara.objects;

import java.util.ArrayList;
import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import com.k_tat.aksara.main.MainGameView;

public class Asteroid {
	protected MainGameView game;
	protected ArrayList<Bitmap> images;
	protected Bitmap image;
	protected Random random;
	private int speed;
	protected int curX, curY;
	protected Matrix mtxMove, mtxRot, mtx; //untuk rotasi
	protected int rotSpeed; //dalam degree
	protected int curRotation;
	protected Rect rect;
	protected int delayTime, initDelayTime;
	protected boolean isVisible;
	private boolean isStopped;
	
	public Asteroid(MainGameView game, int spd, int rot, int delayTime){
		this.game = game;
		images = new ArrayList<Bitmap>();
		for(int i=0;i<4;i++){
			images.add(game.getBitmap("asteroid_enemy" + i)); 
		}
		random = new Random();
		image = images.get(random.nextInt(4));
		curX = random.nextInt((int) game.getStandardWidth());
		curY = -image.getHeight();
		speed = spd;
		rotSpeed = rot;
		curRotation = 0;
		mtxMove = new Matrix();
		mtxRot = new Matrix();
		mtx = new Matrix();
		rect = new Rect(curX, curY, curX+image.getWidth(), curY+image.getHeight());
		isVisible = false;
		initDelayTime = delayTime;
		this.delayTime = initDelayTime;
		isStopped = false;
	}
	
	public int getCurX() {
		return curX;
	}
	
	public void setCurX(int curX) {
		this.curX = curX;
	}
	
	public void reset() {
		if(isStopped)
			return;
		speed++;
		image = images.get(random.nextInt(4));
		curY = -image.getHeight() - 10;
		curX = random.nextInt((int) game.getStandardWidth() - image.getWidth());
		mtxMove.setTranslate(curX, curY);
		mtxRot.setRotate(curRotation, image.getWidth()/2, image.getHeight()/2);
		mtx.setConcat(mtxMove, mtxRot);
		rect.set(curX, curY, curX+image.getWidth(), curY+image.getHeight());
		delayTime = initDelayTime; //500 frame maksudnya, kira 60 FPS, berarti kira2 300/60 = ~5s
	}
	
	public void update(){
		if(delayTime > 0){
			delayTime--;
			return;
		} else
			isVisible = true;
		
		if(curY >= game.getStandardHeight()){
			reset();
		}
		curRotation = (curRotation + rotSpeed) % 360;
		
		curY += speed;
		
		mtxMove.setTranslate(curX, curY);
		mtxRot.setRotate(curRotation, image.getWidth()/2, image.getHeight()/2);
		mtx.setConcat(mtxMove, mtxRot);
		rect.set(curX, curY, curX+image.getWidth(), curY+image.getHeight());
	}
	
	public void draw(Canvas canvas, Paint paint){
		if(isVisible)
			canvas.drawBitmap(image, mtx, paint);
	}
	
	public Rect getRect() {
		return rect;
	}

	public int getDamage() {
		return 25;
	}

	public void stop() {
		isStopped = true;
	}
	
}
