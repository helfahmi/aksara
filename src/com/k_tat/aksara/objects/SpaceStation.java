package com.k_tat.aksara.objects;

import com.k_tat.aksara.main.MainGameView;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class SpaceStation {
	private enum STATE{
		IDLE, MOVING, STOP
	}
	private MainGameView game;
	private Bitmap image;
	private int x,y;
	private boolean isVisible, isCompleted;
	private STATE curState;
	
	public SpaceStation(MainGameView game){
		image = game.getBitmap("space_station");
		x  = (int) ((game.getStandardWidth() - game.getBitmap("space_station").getWidth())/2);
		y = -image.getHeight();
		isVisible = false;
		isCompleted = false;
		curState = STATE.IDLE;
	}
	
	public void startMove(){
		isVisible = true;
		curState = STATE.MOVING;
	}
	
	public boolean isCompleted(){
		return isCompleted;
	}
	
	public void update(){
		if(curState == STATE.MOVING){
			isVisible = true;
			if(y < 150){
				y += 2;
			} else
				isCompleted = true;
		}
	}
	
	public void draw(Canvas canvas, Paint paint){
		canvas.drawBitmap(image, x, y, null);
	}
}
