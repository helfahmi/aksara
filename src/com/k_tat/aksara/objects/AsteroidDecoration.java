package com.k_tat.aksara.objects;

import com.k_tat.aksara.main.MainGameView;

public class AsteroidDecoration extends Asteroid {

	public AsteroidDecoration(MainGameView game, int spd, int rot, int delayTime) {
		super(game, spd, rot, delayTime);
		for (int i = 0; i < 2; i++) {
			images.set(i, game.getBitmap("asteroid" + i));
		}
		for (int i = 0; i < 2; i++) {
			images.remove(images.size() - 1);
		}
		reset();
	}
	
	@Override
	public void reset() {
		image = images.get(random.nextInt(2));
		curY = -image.getHeight() - 10;
		curX = random.nextInt((int) game.getStandardWidth() - image.getWidth());
		mtxMove.setTranslate(curX, curY);
		mtxRot.setRotate(curRotation, image.getWidth()/2, image.getHeight()/2);
		mtx.setConcat(mtxMove, mtxRot);
		rect.set(curX, curY, curX+image.getWidth(), curY+image.getHeight());
		delayTime = initDelayTime; //500 frame maksudnya, kira 60 FPS, berarti kira2 300/60 = ~5s
	}
	
	@Override
	public int getDamage() {
		return 0;
	}
}
