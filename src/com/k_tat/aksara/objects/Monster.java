package com.k_tat.aksara.objects;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.screens.GameScreen;

public class Monster {
	private enum STATE {
		WAITING, FLYING, BUBBLING, BUBBLED, DESTROYING, DESTROYED, END;
	};

	private Animation curAnimation;
	private Animation monster, explode, bubble, bubbling;
	private int health;
	private MainGameView game;
	private Rect rect;
	private int speed; // sumbu-y
	private int counter;
	private Random random;
	private boolean moving, exploding;
	private STATE currentState;
	private int delayTime, initialMonsterDelay;
	private boolean isVisible;
	private GameScreen parent;
	private boolean isFinished;

	public Monster(MainGameView game, GameScreen parent, String bitmapName,
			int speed, int monsterDelay) {
		int x, y;

		/* Bitmap */
		Bitmap bitmap_normal = game.getBitmap(bitmapName);
		Bitmap bitmap_explode = game.getBitmap(bitmapName + "_explode");
		Bitmap bitmap_bubbling = game.getBitmap(bitmapName + "_bubble");
		String monsterColor = bitmapName.split("_")[1];
		Bitmap bitmap_bubble = game.getBitmap("bubble_" + monsterColor);

		this.parent = parent;
		counter = 1;
		health = 100;
		isFinished = false;
		random = new Random();
		x = random.nextInt((int) game.getStandardWidth()
				- bitmap_normal.getWidth());
		y = -bitmap_normal.getHeight();
		explode = new Animation(bitmap_explode, x, y, 1, 8);
		explode.setLooping(false);
		explode.stop();

		// Log.d("harits", "Monster ctr (wxh): "+ bitmap_normal.getWidth() + " "
		// + bitmap_normal.getHeight());
		monster = new Animation(bitmap_normal, x, y, 1, 8);

		bubbling = new Animation(bitmap_bubbling, x, y, 1, 8);
		bubbling.setLooping(false);
		bubbling.stop();

		bubble = new Animation(bitmap_bubble, x, y, 1, 8);

		this.game = game;
		this.speed = speed;
		rect = new Rect(x, y, x + monster.getWidth(), y + monster.getHeight());
		initialMonsterDelay = monsterDelay;
		delayTime = initialMonsterDelay;
		init();
	}

	public boolean isFinished(){
		return isFinished;
	}
	
	public void init() {
		curAnimation = monster;
		moving = true;
		isVisible = false;
		exploding = false;
		currentState = STATE.WAITING;
	}

	public Animation getAnimation() {
		return monster;
	}

	public boolean isExploding() {
		return exploding;
	}

	public void move() {
		moving = true;
	}

	public static void loadBitmap(MainGameView game, String name) {
		game.getBitmap(name);
		game.getBitmap(name + "_explode");
		game.getBitmap(name + "_bubble");
		String monsterColor = name.split("_")[1];
		game.getBitmap("bubble_" + monsterColor);
	}

	public void stop() {
		moving = false;
	}
	
	public void finish(){
		isFinished = true;
		currentState = STATE.END;
	}

	public void beBubble() {
		if (getY() >= 0 && (getY() + curAnimation.getHeight()) <= game.getStandardHeight() && currentState == STATE.FLYING) {
			bubbling.setX(curAnimation.getX());
			bubbling.setY(curAnimation.getY());
			curAnimation = bubbling;
			currentState = STATE.BUBBLING;
			curAnimation.start();
		}
	}

	public void explode() {
		if (getY() >= 0 && (getY() + curAnimation.getHeight()) <= game.getStandardHeight() && currentState == STATE.FLYING) {
			stop();
			/*
			 * Log.d("harits", "monster X: " + monster.getX()); Log.d("harits",
			 * "monster width: " + monster.getWidth()); Log.d("harits",
			 * "-------"); Log.d("harits", "explosion X: " + explode.getX());
			 * Log.d("harits", "explosion width: " + explode.getWidth());
			 */
			exploding = true;
			explode.setX(monster.getX() + monster.getHeight() / 2
					- explode.getHeight() / 2);
			explode.setY(monster.getY() + monster.getHeight() / 2
					- explode.getHeight() / 2);
			curAnimation = explode;
			currentState = STATE.DESTROYED;
			explode.start();
		}
	}

	public float getX() {
		return curAnimation.getX(); // titik acuannya jadi ditengah
	}

	public float getY() {
		return curAnimation.getY(); // titik acuannya jadi ditengah
	}

	public void reset() {
		
		currentState = STATE.END;
		curAnimation = monster;
		curAnimation.setX(random.nextInt((int) game.getStandardWidth()
				- curAnimation.getWidth()));
		curAnimation.setY(-curAnimation.getHeight());
		rect.set((int) curAnimation.getX(), (int) curAnimation.getY(),
				(int) (curAnimation.getX() + curAnimation.getWidth()),
				(int) (curAnimation.getY() + curAnimation.getHeight()));
		delayTime = initialMonsterDelay; // 500 frame maksudnya, kira 60 FPS,
											// berarti kira2 500/60 = ~8s
	}

	public void draw(Canvas canvas, Paint paint) {
		if (isVisible)
			curAnimation.draw(canvas, paint);
	}

	public void update() {
		if (currentState == STATE.WAITING) {
			if (delayTime > 0) {
				delayTime--;
				return;
			} else {
				isVisible = true;
				currentState = STATE.FLYING;
			}
		} else if (currentState == STATE.FLYING) {
			if (moving) {
				curAnimation.setY(curAnimation.getY() + speed);
				if (curAnimation.getX() <= parent.getRocket().getX() - 2)
					curAnimation.setX(curAnimation.getX() + 1);
				else if (curAnimation.getX() >= parent.getRocket().getX() + 2)
					curAnimation.setX(curAnimation.getX() - 1);
			}
			if (curAnimation.getY() >= game.getStandardHeight()) {
				reset();
			}
			rect.set((int) curAnimation.getX(), (int) curAnimation.getY(),
					(int) (curAnimation.getX() + curAnimation.getWidth()),
					(int) (curAnimation.getY() + curAnimation.getHeight()));
		} else if (currentState == STATE.DESTROYED) {
			if (curAnimation.isFinished()) {
				currentState = STATE.END;
				/*curAnimation = monster;
				reset();
				explode.reset();
				init();*/
			}
		} else if (currentState == STATE.BUBBLING) {
			// curAnimation.setY(monster.getY() + speed);

			if (curAnimation.getY() >= game.getStandardHeight()) {
				reset();
			}
			rect.set((int) curAnimation.getX(), (int) curAnimation.getY(),
					(int) (curAnimation.getX() + curAnimation.getWidth()),
					(int) (curAnimation.getY() + curAnimation.getHeight()));

			if (curAnimation.isFinished()) {
				currentState = STATE.BUBBLED;
			}
		} else if (currentState == STATE.BUBBLED) {
			curAnimation.setY(curAnimation.getY() + speed);

			if (curAnimation.getY() >= game.getStandardHeight()) {
				reset();
			}
			rect.set((int) curAnimation.getX(), (int) curAnimation.getY(),
					(int) (curAnimation.getX() + curAnimation.getWidth()),
					(int) (curAnimation.getY() + curAnimation.getHeight()));
		} else if(currentState == STATE.END){
			isVisible = false;
			isFinished = true;
		}

		counter++;
		if (counter % 8 == 0) {
			curAnimation.update();
			counter = 1;
		}
	}

	public boolean isBubble() {
		return (currentState == STATE.BUBBLED || currentState == STATE.BUBBLING);
	}

	public Rect getRect() {
		return rect;
	}

	public void hit(int damage) {
		health -= damage;
	}

	public int getHealth() {
		return health;
	}
}
