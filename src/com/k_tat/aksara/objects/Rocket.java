package com.k_tat.aksara.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.k_tat.aksara.main.MainGameView;

public class Rocket {
	private enum STATE{
		NORMAL, END
	}
	private Animation rocket;
	private MainGameView game;
	private float maxSpeed;
	private float speed;
	private float friction;
	private Rect rect;
	private int counter;
	private STATE curState;
	private boolean isRocketFinished;
	
	public Rocket(MainGameView game, int x, int y){
		rocket = new Animation(game.getBitmap("rocket"), x, y, 4, 6);
		this.game = game;
		this.maxSpeed = 5;
		speed = 0;
		friction = (float) 0.999995;
		rect = new Rect((int)rocket.getX(), (int)rocket.getY(), (int)rocket.getX()+rocket.getWidth(), (int)rocket.getY()+170);
		counter = 0;
		curState = STATE.NORMAL;
		isRocketFinished = false;
	}
	
	public boolean isRocketFinished(){
		return isRocketFinished;
	}
	
	public Animation getAnimation() {
		return rocket;
	}
	
	public void endRocket(){
		curState = STATE.END;
	}
	
	public float getX(){
		return rocket.getX();
	}
	
	public float getY(){
		return rocket.getY();
	}
	
	public void draw(Canvas canvas, Paint paint) {
		rocket.draw(canvas, paint);
	}
	
	public void update(float accelX){
		
		if(curState == STATE.NORMAL){
			speed += accelX;
			if(speed > maxSpeed)
				speed = maxSpeed;
			if(speed < -maxSpeed)
				speed = -maxSpeed;
			
			
			//Log.d("harits", "speed before friction: " + speed);
			
			speed *= friction;
			
			/*
			Log.d("harits", "speed after friction: " + speed);
			Log.d("harits", "accel: " + accelX);
			Log.d("harits", "rocket x: " + rocket.getX());
			*/
			
			rocket.setX(rocket.getX() - speed);
			
			
			if(rocket.getX() < 0){
				rocket.setX(0);
				speed = 0;
			}
			if(rocket.getX() + rocket.getWidth() > game.getStandardWidth()){
				rocket.setX((int)  game.getStandardWidth() - rocket.getWidth());
				speed = 0;
			}
		} else if(curState == STATE.END){
			if(rocket.getX() < 195)
				rocket.setX(rocket.getX() + 1);
			else if(rocket.getX() > 195)
				rocket.setX(rocket.getX() - 1);
			else if(rocket.getY() > 190){
				rocket.setY(rocket.getY() - 2);
			} else {
				isRocketFinished = true;//DONE! TODO: Push Success Screen, save level cleared.
			}
		}

		if(counter < 10){
			counter++;
		} else {
			counter = 0;
			rocket.update();
		}
		
		rect.set((int)rocket.getX(), (int)rocket.getY(), (int)rocket.getX()+rocket.getWidth(), (int)rocket.getY()+rocket.getHeight());
	}
	
	public Rect getRect() {
		return rect;
	}
}
