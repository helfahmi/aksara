package com.k_tat.aksara.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.system.Touchable;

public class BitmapButton implements Touchable {
	private TouchListener action;
	private MainGameView game;
	private Bitmap bitmapTouched, bitmapUntouched;
	private float x, y;
	private boolean touched;
	private boolean enable = true;
	private boolean visible = true;
	private int width;
	private Object obj;
	
	public interface TouchListener {
		public void onTouchDown(MainGameView game, MotionEvent event);
		public void onTouchUp(MainGameView game, MotionEvent event, Object obj);
		public void onTouchMove(MainGameView game, MotionEvent event);
	}
	
	public BitmapButton(MainGameView game, Bitmap bitmapUntouched, Bitmap bitmapTouched, float x, float y, Object data){
		this.game = game;
		this.x = x;
		this.y = y;
		this.obj = data;
		this.bitmapTouched = bitmapTouched;
		this.bitmapUntouched = bitmapUntouched;
		width = bitmapTouched.getWidth();
		touched = false;
	}
	
	public void setWidth(int w) {
		width = w;
	}
	
	public Bitmap getBitmapTouched () {
		return bitmapTouched;
	}
	
	public Bitmap getBitmapUntouched () {
		return bitmapUntouched;
	}
	
	public void setBitmapTouched(Bitmap _bitmap) {
		bitmapTouched = _bitmap;
	}
	
	public void setBitmapUntouched(Bitmap _bitmap) {
		bitmapUntouched = _bitmap;
	}
	
	public void draw(Canvas canvas){
		if(visible){
			if(touched){
				//Log.d("harits", "drawing touched");
				canvas.drawBitmap(bitmapTouched, new Rect(0,0,width, bitmapTouched.getHeight()), new RectF(x,y,x+width,y+bitmapTouched.getHeight()), null);
			} else {
				//Log.d("harits", "drawing untouched");
				canvas.drawBitmap(bitmapUntouched, new Rect(0,0,width, bitmapUntouched.getHeight()), new RectF(x,y,x+width,y+bitmapUntouched.getHeight()), null);
			}
		}
	}
	
	public void addTouchListener(TouchListener action){
		this.action = action;
	}
	
	public boolean onTouchEvent(MotionEvent event, float mag, float dx, float dy) {
		if(!enable) return false;
		
		final int actioncode = event.getAction() & MotionEvent.ACTION_MASK;
		
		RectF r = new RectF((x + dx)*mag, y*mag, (x + dx + bitmapUntouched.getWidth())*mag,  (y + bitmapUntouched.getHeight())*mag);
		
		if (r.contains(event.getX(), event.getY())){
			switch (actioncode) {
				case MotionEvent.ACTION_DOWN:
					//Log.d("harits", "masuk action down");
					action.onTouchDown(game,event);
					touched = true;
					break;
				case MotionEvent.ACTION_MOVE:
					//Log.d("harits", "masuk action move");
					action.onTouchMove(game,event);
					break;
				case MotionEvent.ACTION_UP:
					//Log.d("harits", "masuk action up");
					action.onTouchUp(game,event,obj);
					touched = false;
					break;		
			}
			return true;
		}else{
			if(touched){
				switch (actioncode) {
				case MotionEvent.ACTION_MOVE:
					action.onTouchUp(game,event,obj);
					touched = false;
					break;
				case MotionEvent.ACTION_UP:
					action.onTouchUp(game,event,obj);
					touched = false;
					break;		
				}
			}
			return false;
		}
	}

	public boolean getTouched() {
		return touched;
	}
	
	public void setTouched(boolean b) {
		touched = b;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		//setEnable(visible);
	}
}
