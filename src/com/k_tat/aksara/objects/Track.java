package com.k_tat.aksara.objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;

import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.models.Aksara;
import com.k_tat.aksara.screens.GameScreen;

public class Track{
	
	private class CheckPointInstance{
		private int x, y;
		private Bitmap bitmap;
		private boolean isVisible;
		public CheckPointInstance(Bitmap bitmap, int x, int y){
			this.bitmap = bitmap;
			this.x = x;
			this.y = y;
			setVisible(true);
		}
		
		public void setPos(int x, int y){
			this.x = x;
			this.y = y;
		}
		
		public int getX(){
			return x;
		}
		
		public int getY(){
			return y;
		}
		
		public void setVisible(boolean val){
			isVisible = val;
		}
		
		public void draw(Canvas canvas, Paint paint){
			//Log.d("harits", "check point coordinates: " + (x + bitmap.getWidth()/2) + " " +  (y + bitmap.getHeight()/2));
			if(isVisible)
				canvas.drawBitmap(bitmap, x - bitmap.getWidth()/2, y - bitmap.getHeight()/2, paint);
		}
		
		public void finish(){
			isVisible = false;
		}
	}
	
	
	private enum STATE{
		INITSPEAK, SPEAKING, ENDSPEAK, NORMAL;
	};
	
	private STATE curState;
	private int tolerance;
	private int accuracy;
	private int MAX_TOLERANCE = 40;
	private Canvas drawn;
	private Paint paint;
	private Bitmap slider, huruf, circle;
	private MainGameView game;
	private Matrix circleMatrixMove, circleMatrixRot, circleMatrixCon;
	private int circleRotSpeed, circleRot;
	private int curX, curY, circleX, circleY;
	private Aksara aksaraData;
	private ArrayList<Aksara> aksaras;
	private GameScreen parent;
	private CheckPointInstance cpInst;
	private boolean isVisible, isTouchable;
	private Random random;
	private int choosenAksara;
	private Rect drawnCanvasBound;
	
	
	//color v2: -16745277
	
	private final int aksaraColor = -16745277; //light blue-ish
	
	@SuppressLint("NewApi")
	public Track(MainGameView game, GameScreen gs, ArrayList<Aksara> aks){
		this.game = game;
		parent = gs;
		aksaras = aks;
		
		curState = STATE.NORMAL;
		
		random = new Random();
		circleMatrixCon = new Matrix();
		circleMatrixMove = new Matrix();
		circleMatrixRot = new Matrix();
		
		tolerance = MAX_TOLERANCE;
		isTouchable = true;
		choosenAksara = random.nextInt(aksaras.size());
		aksaraData = aksaras.get(choosenAksara);
		
		this.huruf = convertToMutable(game.getBitmap(aksaraData.getName()));
		
		/*** ACCURACY ***/
		
		accuracy = 99; //Dalam persen. Awalnya masih 100%.
		
		/*** ACCURACY END ***/
		
		/*** CIRCLE ***/
		
		slider = game.getBitmap("trail");
		circle = game.getBitmap("galaxy");
		
		circleX = (int) ((game.getStandardWidth() - circle.getWidth())/2);
		circleY = 100;
		
		circleMatrixMove.setTranslate(circleX, circleY);
		circleRot = 0;
		circleRotSpeed = -1;
		
		curX = circleX - huruf.getWidth()/2 + circle.getWidth()/2;
		curY = circleY - huruf.getHeight()/2 + circle.getHeight()/2;
		
		/*** CIRCLE END ***/
		
		drawn = new Canvas();
		drawn.setBitmap(this.huruf);
		drawnCanvasBound = drawn.getClipBounds();
		paint = new Paint();
		paint.setColor(Color.RED);
		aksaraData.reset();
		cpInst = new CheckPointInstance(game.getBitmap("checkpoint"), aksaraData.getCurrentCheckPoint().getX() + curX, aksaraData.getCurrentCheckPoint().getY() + curY);
		/*
		Log.d("harits", "cpX cpY: " + aksaraData.getCurrentCheckPoint().getX() + " " + aksaraData.getCurrentCheckPoint().getY());
		Log.d("harits", "curX curY: " + curX + " " + curY);
		Log.d("harits", "checkpoint X Y: " + cpInst.getX() + " " + cpInst.getY());
		*/
		isVisible = false; 
		//complete = false; //track belum selesai ditulis
		//paint.setXfermode(new PorterDuffXfermode(Mode.SCREEN));
		//paint.setShader(new BitmapShader(game.getBitmap("slider"), TileMode.CLAMP, TileMode.CLAMP));
	}
	
	public void setHuruf(String name){
		this.huruf = convertToMutable(game.getBitmap(aksaraData.getName()));
		drawn = new Canvas();
		drawn.setBitmap(this.huruf);
		//drawn.drawRect(curX, curY, curX + huruf.getWidth(), curY + huruf.getHeight(), paint);
	}
	
	public File getDir(Context context){
		File cacheDir;
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			//Log.d("harits", "SD card is mounted");
            cacheDir = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");
		} else {
			//Log.d("harits", "SD card is not mounted, using cache");
            cacheDir = new File(context.getCacheDir() + File.separator + "temp.tmp");
		}
           return cacheDir;
	}
	
	public Bitmap convertToMutable(Bitmap imgIn) {
        Bitmap imgOut = null;
		try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            //File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");
			/*
			if(game == null){
				Log.d("harits", "game is null");
			} else
				Log.d("harits", "game is NOT null");
			
			if(game.getContext() == null){
				Log.d("harits", "game context is null");
			} else
				Log.d("harits", "game context is NOT null");
			*/
			File file = getDir(game.getContext());
            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
 
            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();
 
            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            //imgIn.recycle();
            //System.gc();
            // try to force the bytes from the imgIn to be released
 
            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgOut = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgOut.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();
 
            // delete the temp file
            file.delete();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgOut;
    }
	
	public void update(){
		circleRot += circleRotSpeed % 360;
		circleMatrixRot.setRotate(circleRot, circle.getWidth()/2, circle.getHeight()/2);
		circleMatrixCon.setConcat(circleMatrixMove, circleMatrixRot);
		
		//if(complete)
		if(curState == STATE.INITSPEAK){
			aksaraData.getSound().play();
			curState = STATE.SPEAKING;
		} else if(curState == STATE.SPEAKING){
			//Log.d("harits", "sound is playing: " + aksaraData.getSound().isPlaying());
			if(!aksaraData.getSound().isPlaying()){//selesai
				aksaraData.getSound().stop();
				curState = STATE.ENDSPEAK;
			}
		} else if(curState == STATE.ENDSPEAK){
			//complete = false;
			bubbleMonster();
			
			/*** Evaluate Accuracy ***/
			accuracy = (int) (accuracy*(tolerance/(float) MAX_TOLERANCE));
			
			parent.setAccuracyUI(accuracy);

			changeAksara();
			resetTrack();
			resetCheckPoint();
			isVisible = false;
			
			curState = STATE.NORMAL;
		}
}
	
	public void clean(){
	/* harus dipastikan bahwa si huruf udah nggak dipake lagi */
		huruf.recycle();
	}
	
	public void getColorCode(int x, int y){
		try{
			Log.d("harits", "color: " + huruf.getPixel(x, y)); 
		} catch(Exception e){
			
		}
	}
	
	public void resetCheckPoint(){
		aksaraData.reset();
		cpInst.setPos(aksaraData.getCurrentCheckPoint().getX() + curX, aksaraData.getCurrentCheckPoint().getY() + curY);
	}
	
	public void nextCheckPoint(){
		aksaraData.nextCheckPoint();
		//Log.d("harits", "lewat sini 1");
		if(!aksaraData.isFinished())
			cpInst.setPos(aksaraData.getCurrentCheckPoint().getX() + curX, aksaraData.getCurrentCheckPoint().getY() + curY);
		//Log.d("harits", "lewat sini 2");
	}
	
	public void recycle(){
		
	}
	
	public void bubbleMonster(){
		parent.bubbleMonster();
	}
	
	public void draw(Canvas canvas, Paint paint){
		if(!isVisible)
			return;
		try {
			//int circleX = curX + huruf.getWidth()/2 - circle.getWidth()/2;
			//int circleY = curY + huruf.getHeight()/2 - circle.getHeight()/2;
			
			canvas.drawBitmap(circle, circleMatrixCon, paint);
			
			if(huruf == null)
				return;
			//debugging VVV
			//canvas.drawRect(curX, curY, curX + drawnCanvasBound.right, curY + drawnCanvasBound.bottom, this.paint);
			canvas.drawBitmap(huruf, curX, curY, paint);
			if(!aksaraData.isFinished())
				cpInst.draw(canvas, paint);
		} catch (Exception err){
			err.printStackTrace();
		}
		//canvas.drawBitmap(checkPointResource, aksaraData.getCurrentCheckPoint().getX(), aksaraData.getCurrentCheckPoint().getY(), null);
	}
	
	public boolean isAllowed(boolean onTrack){
		if(onTrack){
			return true;
		} else {
			if(tolerance > 0){
				tolerance--;
				return true;
			} else
				return false;
		}
	}
	
	public void processFinish(){
		curState = STATE.INITSPEAK;
	}
	
	public int getAccuracy(){
		return accuracy;
	}
	
	public void onTouchEvent(MotionEvent e, float mag, float dx, float dy) {
		if(!isTouchable)
			return;
		final int actioncode = e.getAction() & MotionEvent.ACTION_MASK;	
		//warna karakter aksara: 	-9058832
		//warna transparan:			0
		
		//koordinat drawn Canvas relatif terhadap canvas main
		int relX, relY;
		int hurufX, hurufY;
		relX = (int) (e.getX()/mag-slider.getWidth()/2 - curX - dx);
		relY = (int) (e.getY()/mag-slider.getHeight()/2 - curY - dy);
		hurufX = (int) (e.getX()/mag - curX - dx);
		hurufY = (int) (e.getY()/mag - curY - dy);
		
		boolean onTrack, onTrack2;
		try {
			//Log.d("harits", "colorTouchTest: " + game.getBitmap(aksaraData.getName()).getPixel(hurufX, hurufY));
			onTrack = (game.getBitmap(aksaraData.getName()).getPixel(hurufX, hurufY) == aksaraColor);
			//onTrack2 = (huruf.getPixel(hurufX, hurufY) == aksaraColor);
			
			//Log.d("harits", "onTrack pake bitmap asli: " + onTrack + ", onTrack2 pake bitmap mutable: " + onTrack2);
			//onTrack = true;
		} catch(Exception error){
			onTrack = false;
		}
		
		int circleX = (int) (curX + huruf.getWidth()/2);
		int circleY = (int) (curY + huruf.getHeight()/2);
		
		
		if(actioncode == MotionEvent.ACTION_DOWN){
			
			if(isVisible){
				//Log.d("harits", "touching points: " + (e.getX()/mag - dx)  + " " + e.getY()/mag);
				//Log.d("harits", "titik tengah lingkaran: " + circleX + " " + circleY);
				float x = (e.getX()/mag - dx) - circleX;
				float y = e.getY()/mag - circleY;
				float touchVal = x*x + y*y;
				//Log.d("harits", "" + touchVal + " > " + 40000);
				if(touchVal > 240*240){
					isVisible = false;
					return;
				}
			} else {
				isVisible = true;
				return;
			}
		}
		
		switch (actioncode) {
			case MotionEvent.ACTION_DOWN:
				/*
				try {
					Log.d("harits", "[DOWN] Kode pixel yang ditekan: " + game.getBitmap("hijaiyyah_ba").getPixel((int) hurufX, (int)hurufY));
				} catch(Exception error) {
					Log.d("harits", "yang Anda klik out of bounds di bitmap 'huruf'");
				}
				*/
				
				/*if on track
				 	if downCheckpoint
				 		paint huruf
				 		pindah ke checkpoint selanjutnya
				 	else 
				 		fail: recycle, restart painting, restart checkpoint
				 else
				 	fail: recycle, restart painting, restart checkpoint
				 */
				try{
					if(isAllowed(onTrack)){
						//Log.d("harits", "Koordinat kepencet di huruf: " + hurufX + ", " + hurufY);
						if(aksaraData.isOnCheckPoint(hurufX, hurufY, "DOWN")){
							//Log.d("harits", "Kena CP DOWN");
							nextCheckPoint();
							
							if(aksaraData.isFinished()){
								processFinish();
							}
							drawn.drawBitmap(slider, relX, relY, paint);
						} else {
							//Log.d("harits", "Tidak kena CP DOWN");
							resetTrack();
							resetCheckPoint();
						}
					} else {
						//Log.d("harits", "Tidak kena track");
						resetTrack();
						resetCheckPoint();
					}
				} catch(Exception error){
					
				}
				break;
			case MotionEvent.ACTION_MOVE:
				/*if on track
				  	if movingCheckpoint 
				  		paint
				  	pindah ke check point selanjutnya
				  else if not on track
				  	fail: recycle, restart painting, restart checkpoint
				 */
				try{
					if(isAllowed(onTrack)){
						if(aksaraData.isOnCheckPoint(hurufX, hurufY, "MOVE")){
							//Log.d("harits", "kena checkpoint MOVE");
							nextCheckPoint();
							drawn.drawBitmap(slider, relX, relY, paint);
						} else {
							drawn.drawBitmap(slider, relX, relY, paint);
						}
					} else {
						//isTouchable = false;
						resetTrack();
						
						resetCheckPoint();
					}
				} catch (Exception err){
					err.printStackTrace();
				}
				
				break;
			case MotionEvent.ACTION_UP:
				/*if checkpoint
					if not checkpoint terakhir
						if upCheckpoint
							pindah ke check point selanjutnya
					else checkpoint terakhir
						finish! :D
				  else not checkpoint
				  	fail: recycle, restart painting, restart checkpoint
				*/
				if(isAllowed(onTrack)){
					if(aksaraData.isOnCheckPoint(hurufX, hurufY, "UP")){
						//Log.d("harits", "Kena checkpoint UP");
						nextCheckPoint();
						if(aksaraData.isFinished()){
							//Log.d("harits", "That was the last [UP] CP! Done!");
							processFinish();
						}
					} else {
						resetTrack();
						resetCheckPoint();
					}
				} else {
					resetTrack();
					resetCheckPoint();
				}
					
				break;		
		}
	}
	
	/* Kalo udah selesai ngerjain aksara, ganti aksara yang dimaenin sekarang pake fungsi ini */
	public void changeAksara(){
		choosenAksara = random.nextInt(aksaras.size());
		aksaraData = aksaras.get(choosenAksara);
	}
	
	public void resetTrack(){
		Bitmap lastHuruf = huruf;
		tolerance = MAX_TOLERANCE;
		setHuruf(aksaraData.getName());
		curX = circleX - huruf.getWidth()/2 + circle.getWidth()/2;
		curY = circleY - huruf.getHeight()/2 + circle.getHeight()/2;
		lastHuruf.recycle();
	}

	public void stop() {
		isTouchable = false;
		isVisible = false;
	}
}
