package com.k_tat.aksara.objects;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.k_tat.aksara.main.MainGameView;

public class Sky {
	
	private MainGameView game;
	
	// background resources
	private Bitmap background, background2;
	
	// background moving
	private final double accel = 1;
	private int top;
	private float origin;
	private int speed, rotateSpeed, maxSpeed, delay;
	private Matrix mtxRotatingLeft, mtxRotatingCenter, mtxRotatingRight;
	private boolean isMoving, isRotating, isVisible;
	
	// background changing
	private boolean isGlowing;
	private int counter;
	
	// sky object: star, meteor, ..., planet
	private Star star;
	private ArrayList<AsteroidDecoration> asteroidDecoration;
	private Meteor meteor;
	private Bitmap planet;
	private int planetX, planetY;
	
	public Sky(MainGameView game, String planet, int spd, int rot){
		this.planet = game.getBitmap(planet);
		background = game.getBitmap("background");
		background2 = game.getBitmap("background2");

		this.game = game;
		speed = 0;
		maxSpeed = 0;

		mtxRotatingLeft = new Matrix();
		mtxRotatingCenter = new Matrix();
		mtxRotatingRight = new Matrix();
		
		isMoving = false;
		isRotating = false;
		if (spd != 0) {
			isMoving = true;
			maxSpeed = spd;
		}
		if (rot != 0) {
			isRotating = true;
			rotateSpeed = rot;
		}
		
		isVisible = true;
		
		if(background == null)
			Log.d("harits", "background null!");

		top = background.getHeight()/2;
		origin = 0;
		
		delay = 60;
		
		isGlowing = false;
		counter = 0;
		
		star = new Star(game, 350, 10);
		asteroidDecoration = new ArrayList<AsteroidDecoration>();
		for (int i = 0; i < 4; i++) {
			asteroidDecoration.add(new AsteroidDecoration(game, 1, 2, 25 * (i + 1)));
		}
		meteor = new Meteor(game, game.getBitmap("meteor"), 2);
		
		planetX = 0;
		planetY = (int) (game.getStandardHeight() - this.planet.getHeight());
	}
	
	public void setMoving(boolean state){
		isMoving = state;
		isRotating = !state;
	}
	
	public void setRotating(boolean state) {
		isMoving = !state;
		isRotating = state;
	}
	
	public void setVisible(boolean b){
		isVisible = b;
	}
	
	public void update(){
		//Log.d("harits", "top: " + top);
		if(delay > 0){
			delay--;
			return;
		}
		
		if(isMoving){
			if(speed <= maxSpeed)
				speed += accel;
			
			if(planetY <= game.getStandardHeight())
				planetY += speed;
			
			if(top - speed <= 0)
				top = background.getHeight()/2;
			else
				top -= speed;
		}

		if (isRotating) {
			origin += rotateSpeed * 0.01f;
			if (origin > 45) {
				origin -= 45;
			}
			mtxRotatingLeft.setRotate(origin-45, game.getStandardWidth()/2, game.getStandardHeight());
			mtxRotatingCenter.setRotate(origin, game.getStandardWidth()/2, game.getStandardHeight());
			mtxRotatingRight.setRotate(origin+45, game.getStandardWidth()/2, game.getStandardHeight());
		}
		
		if (counter < 16) {
			counter++;
		} else {
			isGlowing = !isGlowing;
			counter = 0;
		}
		
		star.update();
		for (AsteroidDecoration e: asteroidDecoration) {
			e.update();
		}
		meteor.update();
	}
	
	public void draw(Canvas canvas, Paint paint){
		// background
		canvas.drawColor(Color.BLACK);
		
		if(isVisible && !isRotating){
			canvas.drawBitmap(background, new Rect(0, top, background.getWidth(), top + background.getHeight()/2), new RectF(0, 0, game.getStandardWidth(), game.getStandardHeight()), paint);
			if (isGlowing) {
				canvas.drawBitmap(background2, new Rect(0, top, background.getWidth(), top + background.getHeight()/2), new RectF(0, 0, game.getStandardWidth(), game.getStandardHeight()), paint);
			}
		}
		
		if (isRotating) {
			canvas.drawBitmap(background, mtxRotatingLeft, null);
			canvas.drawBitmap(background, mtxRotatingCenter, null);
			canvas.drawBitmap(background, mtxRotatingRight, null);
			if (isGlowing) {
				canvas.drawBitmap(background2, mtxRotatingLeft, null);
				canvas.drawBitmap(background2, mtxRotatingCenter, null);
				canvas.drawBitmap(background2, mtxRotatingRight, null);
			}
		}

		star.draw(canvas);
		for (AsteroidDecoration e: asteroidDecoration) {
			e.draw(canvas, paint);
		}
		meteor.draw(canvas, null);
		
		// planet/earth/space station
		if (planetY <= game.getStandardHeight()) {
			canvas.drawBitmap(planet, planetX, planetY, null);
		}
	}

	public void stop() {
		isMoving = false;
	}
}
