package com.k_tat.aksara.objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

public class Animation {
	private Bitmap atlas; //bitmap kumpulan bitmap
	private int rowCount, colCount; //jumlah baris, kolom
	private float curX, curY; //posisi objek sekarang thd canvas
	private int curFrame; //sekarang frame keberapa, dari pojok kiri atas kekanan bawah, dimulai dari 0
	private int width, height; //ukuran lebar panjang tiap frame bitmap ini
	public boolean isAnimating, finish, looping;	
	
	public Animation(Bitmap bit, int x, int y, int r, int c){
		atlas = bit;
		curX = x;
		curY = y;
		rowCount = r;
		colCount = c;
		//if(atlas == null)
			//Log.d("harits", "flag_untouched is null");
		width = atlas.getWidth() / colCount;
		height = atlas.getHeight() / rowCount;
		isAnimating = true;
		finish = false;
		looping = true;
		curFrame = 0;
	}
	
	public boolean isFinished(){
		return finish;
	}
	
	public void start(){
		curFrame = 0;
		isAnimating = true;
	}
	
	public void stop(){
		isAnimating = false;
	}
	
	public void setLooping(boolean val){
		looping = val;
	}
	
	public void reset(){
		isAnimating = false;
		finish = false;
		curFrame = 0;
	}
	
	public void update(){
		if(isAnimating){
			curFrame++;
			if(curFrame == rowCount*colCount - 1){
				if(!looping){
					isAnimating = false;
					finish = true;
				} else
					curFrame = 0;
			}
		}
	}
	
	public void setX(float x){
		curX = x;
	}
	
	public void setY(float y){
		curY = y;
	}
	
	public void draw(Canvas canvas, Paint paint){
		int sx = curFrame % colCount;
		sx *= width;
		int sy = curFrame / colCount;
		sy *= height;
		canvas.drawBitmap(atlas, new Rect(sx, sy, sx + width, sy + height), new RectF(curX, curY, curX + width, curY + height), paint);
	}
	
	public void draw(Canvas canvas, Matrix matrix, Paint paint) {
		int sx = curFrame % colCount;
		sx *= width;
		int sy = curFrame / colCount;
		sy *= height;
		canvas.drawBitmap(Bitmap.createBitmap(atlas, sx, sy, width, height), matrix, paint);
	}
	
	public void setAnimating(boolean state){
		isAnimating = state;
	}

	public float getX() {
		return curX;
	}

	public float getY() {
		return curY;
	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getRow(){
		return rowCount;
	}
	
	public int getColumn(){
		return colCount;
	}
	
	public Bitmap getBitmap() {
		return atlas;
	}
	
	public void setBitmap(Bitmap curr_bitmap) {
		atlas = curr_bitmap;
	}
}
