package com.k_tat.aksara.objects;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.MotionEvent;


import com.k_tat.aksara.main.MainGameView;
import com.k_tat.aksara.objects.BitmapButton.TouchListener;
import com.k_tat.aksara.system.Touchable;

public class Flag implements Touchable{
	private TouchListener action;
	private boolean touched;
	private boolean enable = true;
	private boolean visible = true;
	private float x, y;
	
	private int counter;
	private Animation flag;
	private MainGameView game;
	
	public interface TouchListener {
		public void onTouchDown(MainGameView game, MotionEvent event);
		public void onTouchUp(MainGameView game, MotionEvent event);
		public void onTouchMove(MainGameView game, MotionEvent event);
	}
	
	public Flag(MainGameView game, String gameLangCode, int x, int y){
		flag = new Animation(game.getBitmap("flag_untouched_"+game.getGameLang()), x, y, 1, 8);
		this.game = game;
		counter = 0;
		touched = false;
		this.x = x;
		this.y = y;
	}
	
	public void addTouchListener(TouchListener action){
		this.action = action;
	}
	
	public boolean onTouchEvent(MotionEvent event, float mag, float dx, float dy) {
		if(!enable) return false;
		
		final int actioncode = event.getAction() & MotionEvent.ACTION_MASK;	
		
		RectF r = new RectF((x + dx)*mag, y*mag, (x + dx + flag.getWidth())*mag,  (y + flag.getHeight())*mag);
		
		if (r.contains(event.getX(), event.getY())){
			switch (actioncode) {
				case MotionEvent.ACTION_DOWN:
					//Log.d("harits", "masuk action down");
					action.onTouchDown(game,event);
					touched = true;
					break;
				case MotionEvent.ACTION_MOVE:
					//Log.d("harits", "masuk action move");
					action.onTouchMove(game,event);
					break;
				case MotionEvent.ACTION_UP:
					//Log.d("harits", "masuk action up");
					action.onTouchUp(game,event);
					touched = false;
					break;		
			}
			return true;
		}else{
			if(touched){
				switch (actioncode) {
				case MotionEvent.ACTION_MOVE:
					action.onTouchUp(game,event);
					touched = false;
					break;
				case MotionEvent.ACTION_UP:
					action.onTouchUp(game,event);
					touched = false;
					break;		
				}
			}
			return false;
		}
	}
	
	public float getX(){
		return flag.getX();
	}
	
	public float getY(){
		return flag.getY();
	}
	
	public void draw(Canvas canvas) {
		flag.draw(canvas, null);
	}
	
	public void update(){
		flag.setBitmap(game.getBitmap("flag_untouched_"+game.getGameLang()));
		if (counter == game.getFPS()/20) {
			flag.update();
			counter = 0;
		}
		else 
			counter ++;
	}

}