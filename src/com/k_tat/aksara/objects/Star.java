package com.k_tat.aksara.objects;

import android.graphics.Canvas;

import com.k_tat.aksara.main.MainGameView;

public class Star {

	private Animation star;
	private MainGameView game;
	private int counter;
	
	public Star(MainGameView game, int x, int y) {
		this.game = game;
		star = new Animation(game.getBitmap("star"), x, y, 1, 8);
		counter = 0;
	}
	
	public void update() {
		if (counter == game.getFPS()/4) {
			star.update();
			counter = 0;
		} else {
			counter++;
		}
	}
	
	public void draw(Canvas canvas) {
		star.draw(canvas, null);
	}
}
